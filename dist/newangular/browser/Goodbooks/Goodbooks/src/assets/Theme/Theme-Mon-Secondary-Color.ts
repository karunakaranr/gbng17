export class thememonsecondarycolor{
    "Default": "#dbcdef";
    "Blue": "#dee8fa";
    "Pink": "#fec4ce";
    "Red": "#fdd8d6";
    "Aqua": "#d6fdf9";
    "Yellow": "#fcfccc";
    "Teal": "#3ffdfd";
    "Green": "#4dff4d"
  }