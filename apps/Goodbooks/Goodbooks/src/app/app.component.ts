// import { Component } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { RouterOutlet } from '@angular/router';

// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrl: './app.component.scss'
// })
// export class AppComponent {
//   title = 'GBNG17';
// }
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AuthState } from './../../../../../features/common/shared/stores/auth.state';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent  {
  title = 'GoodBooks';
  deviceInfo= null;
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<any>;
  constructor(private store: Store){
  }
 public get loginDto() {
    let retvalue :any= null;
    let check=[] ;
    this.store.selectSnapshot((state) => {
      if (state.AuthStore.DLoginDTO != null)
        retvalue =JSON.stringify(state.AuthStore.DLoginDTO);
        check = JSON.parse(retvalue);
    });
    return retvalue;
  }
}