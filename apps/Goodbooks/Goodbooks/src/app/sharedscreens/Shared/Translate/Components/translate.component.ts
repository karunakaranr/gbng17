import { Component, NgIterable, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslocoService} from '@ngneat/transloco';
@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TranslateComponent implements OnInit  {
  activelang: string | undefined;
  avaliblelang: (string[] & NgIterable<string>) | null | undefined = this.transloco.getAvailableLangs() as (string[] & NgIterable<string>) | null | undefined;;

  constructor(public transloco: TranslocoService){}
  ngOnInit(){
    this.activelang = this.transloco.getActiveLang();
  }
  
  get activeLang() {
    return this.transloco.getActiveLang();
  }

  changelang(lang: string) {
    this.transloco.setActiveLang(lang);
  }
}

