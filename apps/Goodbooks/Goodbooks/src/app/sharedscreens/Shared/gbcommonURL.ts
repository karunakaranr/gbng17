export class URLS {
  public static AUTHENTICATION = '/fws/Server.svc/AuthenticationKey';
  public static USERLOGIN = '/fws/User.svc/Authenticate/?UserCode=';
  public static VERSION  = '/apiu/Version.svc/?UserCode=&ConnectionName='
}
