import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { GbcommonModule } from './../../../../../../../libs/gbcommon/src/lib/gbcommon.module';
import { TranslateModule } from './Translate/Components/translate.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    GbcommonModule,
    TranslateModule,
  ],
  exports: [],
})
export class GbSharedModule { }
