import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, RouterStateSnapshot } from '@angular/router';
import {Router} from '@angular/router';
import { Store, Select } from '@ngxs/store';


// @Injectable({
//   providedIn: 'root'
// })
export const AuthGuard: CanActivateFn = (route, state) => {

  // const currentmenu = route.url[0].path;
  const myRoute = inject(Router);
  const store = inject(Store)
  let retvalue :any =null;
  let authdata= '';
  store.selectSnapshot((state) => {
    if (state.AuthStore.DLoginDTO != null)
      retvalue = state.AuthStore.DLoginDTO;
      authdata = retvalue.UserCode;
  });
  if (authdata) {
    return true;
  }
  myRoute.navigate(['/Login'], { queryParams: { returnUrl: state.url } });
  return false;
};
// export class AuthGuard {


//   constructor(private myRoute: Router, private store: Store){
//   }
//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//     const currentUserSubject = this.getLoginDTO();
//     if (currentUserSubject) {
//         return true;
//     }
//     this.myRoute.navigate(['/Login'], { queryParams: { returnUrl: state.url } });
//     return false;
// }
// public getLoginDTO(): string {
//   let retvalue :any =null;
//   let authdata= '';
//   this.store.selectSnapshot((state) => {
//     if (state.AuthStore.DLoginDTO != null)
//       retvalue = state.AuthStore.DLoginDTO;
//       authdata = retvalue.UserCode;
//   });
//   return authdata;
// }
// }
