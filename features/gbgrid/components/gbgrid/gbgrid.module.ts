import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GbgridComponent } from './gbgrid.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [GbgridComponent],
  imports: [
    CommonModule,
    AgGridModule,
  ],
  exports:[GbgridComponent]
})
export class GbgridModule { }
