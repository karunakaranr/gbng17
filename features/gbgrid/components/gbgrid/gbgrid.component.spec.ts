import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { GbgridComponent } from './gbgrid.component';

describe('GbgridComponent', () => {
  let component: GbgridComponent;
  let fixture: ComponentFixture<GbgridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
