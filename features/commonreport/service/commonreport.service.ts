import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommonReportdbservice } from './../dbservice/commonreportdbservice';
@Injectable({
  providedIn: 'root'
})
export class CommonReportService {
  constructor(public dbservice:CommonReportdbservice) { }

  public Reportdetailservice(id:any){                      //col
    return this.dbservice.picklist(id);
  }

  public Rowservice(obj:any, criteria:any){                    //row
    return this.dbservice.reportData(obj, criteria);
  }

  
  public CriteriaConfigWithFieldValue(id:any) {            //configselection
    return this.dbservice.CriteriaConfigWithFieldValue(id);
  }

  public CriteriaConfigId(id:any) {                        //
    return this.dbservice.CriteriaConfigId(id);
  }

  public reportDatacriteria(data:any, criterias:any): any {            //afterfilter
    return this.dbservice.reportDatacriteria(data, criterias);
  }

  public drilldownsettingservice(drillmenuid:any, selectedrowdata:any ,fieldname:any){  //drilldownsettings
    return this.dbservice.drilldownsettingdb(drillmenuid, selectedrowdata ,fieldname);
  }
}
