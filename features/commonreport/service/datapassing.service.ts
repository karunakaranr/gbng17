// import { Injectable } from '@angular/core';
// import { Subject, Observable, BehaviorSubject } from 'rxjs';

// @Injectable()
// export class SharedService {

//   private sharedData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
//   sharedData$: Observable<any> = this.sharedData.asObservable();

//   constructor() { }

//   setData(updatedData) {
//     console.log("updatedData............",updatedData)
//     this.sharedData.next(updatedData);
//   }


// }


import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommonReportdbservice } from './../dbservice/commonreportdbservice';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  constructor() { }

  //for row data
  private rowselection = new BehaviorSubject<any>({});
  setRowInfo(rowselect: any) {
    this.rowselection.next(rowselect);
  }
  getRowInfo() {
    // console.log("this.rowselection........",this.rowselection)
    return this.rowselection.asObservable();
  }



  //for drilldownpassing
  private drilldownselection = new BehaviorSubject<any>({});
  setDrillInfo(drilldata: any) {
    // console.log("drilldata",drilldata)
    this.drilldownselection.next(drilldata);
  }
  getDrillInfo() {
    // console.log("this.drilldownselection........", this.drilldownselection)
    return this.drilldownselection.asObservable();
  }
}