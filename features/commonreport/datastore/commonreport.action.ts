import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Report';

export class  Reportid{
  static readonly type = '[' + storeSliceName + '] Reportid';
  constructor(public reportid: any) {
  }
}

export class  CriteriaConfigArray{
  static readonly type = '[' + storeSliceName + '] CriteriaConfigArray';
  constructor(public criteriaconfig: any) {
  }
}

export class  ListofViewtypes{
  static readonly type = '[' + storeSliceName + '] ListofViewtypes';
  constructor(public viewtypes: any) {
  }
}


export class  SelectedViewtype{
  static readonly type = '[' + storeSliceName + '] SelectedViewtype';
  constructor(public selectedview: any) {
  }
}

export class  Configid{
  static readonly type = '[' + storeSliceName + '] Configid ';
  constructor(public selectedconfigid: any) {
  }
}

export class  RowDataPassing{
  static readonly type = '[' + storeSliceName + '] RowDataPassing';
  constructor(public rowdatapassing: any) {
  }
}

export class Sourcecriteriadrilldown{
  static readonly type = '[' + storeSliceName + '] Sourcecriteriadrilldown';
  constructor(public sourcecriteriadrilldown: any) {
  }
}

export class Drilldownsetting{
  static readonly type = '[' + storeSliceName + '] Drilldownsetting';
  constructor(public drilldownsetting: any) {
  }
}