import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import {MatDividerModule} from '@angular/material/divider';
import { MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import { GbFilterComponent } from './component/gbfilter/gbfilter.component';
// import { EntitylookupModule } from './../entitylookup/entitylookup.module';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FilterState } from './store/gbfilter.state';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [
    GbFilterComponent,
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatRadioModule,
    // EntitylookupModule,
    MatDatepickerModule,
    NgxsModule.forFeature([FilterState]),
    NgxsFormPluginModule.forRoot(),
  ],
  exports: [
    GbFilterComponent,
    
  ],
})
export class GbfilterModule {
 
}
