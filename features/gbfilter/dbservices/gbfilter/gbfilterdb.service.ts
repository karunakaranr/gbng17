import { Inject, Injectable } from '@angular/core';
import {AuthState} from './../../../common/shared/stores/auth.state';
import { Observable } from 'rxjs';
import { GBHttpService } from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { GBBaseDBService } from './../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { GbFilterURL } from './../../URLS/urls';
import { ReportCriteriaArray } from './../../model/IFilter';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Select, Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root',
})
export class GbFilterdbservice extends GBBaseDBService {

  constructor(@Inject(GBHttpService) private http: GBHttpService) {
    super();
  }
  @Select(AuthState.AddDTO) LOGINDTO$!: Observable<any>;

  public FilterDbServices(id:any) {
    let userid;
    this.LOGINDTO$.subscribe(dto =>{
      let Logindto = dto;
      userid = dto.UserId;
    })
    // return this.http.httpget(GbFilterURL.filter);
    const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId='+userid+'&MenuId='+id;
    return this.http.httpget(url);
  }
  public configration(criteria:any) {
    // return this.http.httpget(GbFilterURL.filter);
    const url = '/fws/CriteriaConfig.svc/';
    return this.http.httppost(url,criteria);
  }

  public reportformatdbservice(reportid:any ,menuid:any){
    const url = '/cs/Criteria.svc/List/?ObjectCode=REPORTVSFORMATS'
    let cri = {
      "SectionCriteriaList": [
        {
          "SectionId": 0,
          "AttributesCriteriaList": [
            {
              "FieldName": "Report.Id",
              "OperationType": 1,
              "FieldValue": reportid,
              "InArray": null,
              "JoinType": 2
            },
            {
              "FieldName": "ApplicableMenuIds",
              "OperationType": 2,
              "FieldValue":menuid,
              "InArray": null,
              "JoinType": 0
            }
          ],
          "OperationType": 0
        }
      ]
    }
    return this.http.httppost(url,cri);
  }

}



