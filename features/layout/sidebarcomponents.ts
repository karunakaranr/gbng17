import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, Type, ViewContainerRef } from '@angular/core';

export interface SideBarComponentType {
  Name: string;
  SBComponent: Type<any>;
}
export class SideBarComponents {
  static bInitialized = false;
  static _components: SideBarComponentType[] = [];
  static _componentRef: ComponentRef<any>;
  static _resolver: ComponentFactoryResolver;

  static initialize(resolver: ComponentFactoryResolver) {
    if (!this.bInitialized) {
      this._resolver = resolver;
      this.bInitialized = true;
    }
  }

  static addComponent(name: string, comp: Type<any>): void {
    this._components.push({Name: name, SBComponent: comp});
  }

  static get Components() {
    return this._components;
  }
  static get CurrentComponent(): SideBarComponentType {
    const name = sessionStorage.getItem("_sidebarcomp");
    var retComps;
    retComps = this._components.filter(o => o.Name === name);
    if (retComps.length > 0)
      return retComps[0];
    else
      return null;
  }

  static CurrentComponentRef(vcr: ViewContainerRef): ComponentRef<any> {
    const factory: ComponentFactory<any> = this._resolver.resolveComponentFactory(this.CurrentComponent.SBComponent);
    var componentRef = vcr.createComponent(factory);
    var componentInstance = componentRef.instance;
    return componentRef;
  }

}
