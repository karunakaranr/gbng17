import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMenulist } from './../../models/Imenu.model';
import { MenudbService } from './../../dbservices/modulelist/menu/menudb.service';
@Injectable({
  providedIn: 'root'
})
export class MenuserviceService {
  constructor(private menudbService: MenudbService) { }

  public savedata(url:any,criteria:any):Observable<any> {
    return this.menudbService.savedata(url,criteria)
  }

  public deletedata(url:any,idfied:any, selectedid:any):Observable<any> {
    return this.menudbService.deletedata(url,idfied, selectedid)
  }
  
  menudetailsservice(moduleid:string): Observable<IMenulist> {

    return this.menudbService.MenulistService(moduleid)
  }

  excelservice(exceldata:any , menudata:any , data:any): Observable<any> {

    return this.menudbService.exceldbservice(exceldata , menudata , data)
  }
}
