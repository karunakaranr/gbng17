import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { formactiondbservice } from './../../dbservices/modulelist/formaction/formactionDB.service';
@Injectable({
  providedIn: 'root',
})
export class formactionservice {

  constructor(public formactionDBservice: formactiondbservice) { }

  public formactionservice(url:any,criteria:any): Observable<any> {
    return this.formactionDBservice.formactionService(url,criteria);
  }

}