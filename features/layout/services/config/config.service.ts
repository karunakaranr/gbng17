import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Configdbservice } from './../../dbservices/modulelist/config/config.db.service';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(public dbservice: Configdbservice) { }
  public CriteriaConfig(obj:any, criteria:any) {
    return this.dbservice.Configdata(obj, criteria);
  }
}