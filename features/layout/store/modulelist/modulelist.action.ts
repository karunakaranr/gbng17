
import { Imoduleid , IMenuDto } from './../../models/Imodule.model';


export class ModuleID {
  static readonly type = '[Imoduleid] set';

  constructor(public moduleid: Imoduleid) {
  }
}

export class MenuOption {
  static readonly type= '[IMenuDto]AddMenu';
  constructor(public Dmenuoption:any){

  }
}
