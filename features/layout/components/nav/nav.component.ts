import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { OpenContextBar } from './../../store/layout.actions';
import { LayoutState } from './../../store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  @Select(LayoutState.CurrentTheme) theme$!: Observable<string>;
  openContext() {
    this.store.dispatch(new OpenContextBar);
  }

}
