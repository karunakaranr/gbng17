import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ReportState } from './../../../../../commonreport/datastore/commonreport.state';
import { LayoutState } from './../../../../store/layout.state';


@Component({
  selector: 'app-gbformdatatitle',
  templateUrl: './gbformdatatitle.component.html',
  styleUrls: ['./gbformdatatitle.component.scss']
})
export class GbformdatatitleComponent implements OnInit {
  viewvalue : any;
  
  @Select(ReportState.SelectedViewtype) selectedview$!: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$!: Observable<string>;
  viewmode:any;
  constructor() { }

  ngOnInit(): void {
    this.visiblebuttons$.subscribe(res => {
      if(res){
        this.viewmode = res;
      }
    })
    this.selectedview$.subscribe(data => {
      if (data){
        this.viewvalue = data.ViewName
      }
    })
  }

}
