import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import {CommonReportService} from './../../../../../commonreport/service/commonreport.service';
import { ReportState } from './../../../../../commonreport/datastore/commonreport.state';
import { Select } from '@ngxs/store';
import { SelectedViewtype} from './../../../../../commonreport/datastore/commonreport.action'
import { Observable } from 'rxjs';
import { LayoutState } from './../../../../store/layout.state';

// const columnselection =  require('./columnselection.json')


@Component({
  selector: 'app-gbformdatarecent',
  templateUrl: './gbformdatarecent.component.html',
  styleUrls: ['./gbformdatarecent.component.scss']
})
export class GbformdatarecentComponent implements OnInit {
  public viewtypes : any;
  public viewmode :any;
  constructor(public store: Store  ,private dataService: CommonReportService ,  public ref : ChangeDetectorRef) { }
  @Select(ReportState.ListofViewtypes) listofview$!: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$!: Observable<string>;
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;

  ngOnInit(): void {
    this.visiblebuttons$.subscribe(res => {
      if(res){
        this.viewmode = res;
        console.log("viewmode",this.viewmode)
      }
    })
    this.listofview$.subscribe(res => {
      if(res){
        this.viewtypes = res;
      }
    })
  }
  // selectedcol: string = '';
  // viewtype : string = '';
  // test : Columnmodel = columnselection; 

  // public selecteddata(datas){
  //   this.selectedcol = datas;i
  //   let data = this.selectedcol;
  //   this.dataService.setColInfo({data});
  // }

  public selectedview(view:any){
    this.store.dispatch(new SelectedViewtype(view)) 
    // this.selectedcol = view;
    // let data = this.selectedcol;
    // this.dataService.setViewInfo({data});
    // this.dataService.setColInfo({data});
  }



  // selectChangeHandler (event: any) {
  //   this.selectedrow = event.target.value;
  //   console.log(this.selectedrow);
  //   let data = this.selectedrow;
  //   this.dataService.setRowInfo({data});
  //   this.store.dispatch(new GbfilterDTO(data ,this.ref));             // should hide on test
  // }
}
