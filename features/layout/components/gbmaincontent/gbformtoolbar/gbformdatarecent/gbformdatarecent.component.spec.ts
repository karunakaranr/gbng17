import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformdatarecentComponent } from './gbformdatarecent.component';

describe('GbformdatarecentComponent', () => {
  let component: GbformdatarecentComponent;
  let fixture: ComponentFixture<GbformdatarecentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformdatarecentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformdatarecentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
