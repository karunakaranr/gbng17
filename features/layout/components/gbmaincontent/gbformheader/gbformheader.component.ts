import { Component, OnInit, ViewChild } from '@angular/core';
import { GbformactionsComponent } from './gbformactions/gbformactions.component';
import { GbformtitleComponent} from './gbformtitle/gbformtitle.component';

@Component({
  selector: 'app-gbformheader',
  templateUrl: './gbformheader.component.html',
  styleUrls: ['./gbformheader.component.scss']
})
export class GbformheaderComponent implements OnInit {
  @ViewChild(GbformtitleComponent) title!: GbformtitleComponent;
  @ViewChild(GbformactionsComponent) forms!: GbformactionsComponent;
  constructor() { }

  ngOnInit(): void {
  }

}
