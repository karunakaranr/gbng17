import { Component, Inject, Input, OnInit, isDevMode } from '@angular/core';
import { GBBasePageComponentNG } from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state';
import { ReportState } from './../../../../../commonreport/datastore/commonreport.state'
// import { FilterState } from './../../../../../gbfilter/store/gbfilter.state'
import { AuthState } from './../../../../../common/shared/stores/auth.state'
import { MenuserviceService } from './../../../../services/menu/menuservice.service'
import { GBHttpService } from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { MatDialog } from '@angular/material/dialog';
// import * as utf8 from "utf8";
import { Router } from '@angular/router';
// import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
// import { GbaboutComponent } from 'features/gbabout/components/gbabout/gbabout.component';
import { ButtonVisibility, FormEditOption, FormEditable, FormUnEditable, Movenextpreviousid, ResetForm } from './../../../../../../features/layout/store/layout.actions';
import { RowDataPassing, SelectedViewtype } from './../../../../../../features/commonreport/datastore/commonreport.action';
import { take } from 'rxjs/operators';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { formactionservice } from './../../../../../../features/layout/services/formaction/formaction.sevice';
// const globalbase64js = require("base-64");
// const globalpako = require('pako');
@Component({
  selector: 'app-gbformactions',
  templateUrl: './gbformactions.component.html',
  styleUrls: ['./gbformactions.component.scss']
})
export class GbformactionsComponent implements OnInit {
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  @Select(ReportState.SelectedViewtype) selectedview$!: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$!: Observable<string>;
  @Select(LayoutState.FormValidationvalues) formvaldiation$!: Observable<string>;
  @Select(ReportState.CriteriaConfigArray) rowcerteria$!: Observable<any>;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$!: Observable<any>;
  // @Select(FilterState.Reportformatselection) reportformatselection$: Observable<any>;
  // @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(AuthState.AddDTO) LOGINDTO$!: Observable<any>;
  @Select(LayoutState.Path) path$!: Observable<any>;
  @Select(LayoutState.FormEdit) edit$!: Observable<boolean>;
  @Select(ReportState.ListofViewtypes) listofview$!: Observable<any>;
  @Select(LayoutState.Versionservicurl) jsaperdetails$!: Observable<any>
  @Select(ReportState.RowDataPassing) rowdatacommon$!: Observable<any>;
  @Input() public _formComponent!: GBBasePageComponentNG;
  @Input() public FormGroup!: GBBasePageComponentNG;
  public viewmode!:any;
  public exceldata!:any;
  public menudetails!:any; 
  public movenextpreid!:any;
  public firstnumber!:number;
  public maxresult:number = 0;
  public MaxResult!:string;
  public maxnum!:number;
  dummy = 1;
  TotalRecord: number = 0;
  public disableButton = true;
  rowData!: any;
  color!: any;;
  public viewtypes : any[] = [];
  public Validation:any;
  public FormReset:any;
  constructor(@Inject(formactionservice) public formaction: formactionservice,public http: GBHttpService,public store: Store, public dialog: MatDialog, private router: Router, public service: MenuserviceService) {
  }

  get formComponent(): GBBasePageComponentNG {
    return this._formComponent;
  }

  get formgrp(): GBBasePageComponentNG {
    return this.FormGroup;
  }
  @Input()
  set formComponent(val: GBBasePageComponentNG) {
    this._formComponent = val;
  }
  public cc = [];
  public rr = [];

  ngOnInit(): void {
    this.store.dispatch(new FormUnEditable);
    this.edit$.subscribe(res => {
      this.disableButton = !res;
    })
    this.rowdatacommon$.subscribe(data => {
      const totalRecordFromStorage = localStorage.getItem('TotalRecord');
      if (totalRecordFromStorage !== null) {
        this.TotalRecord = parseInt(totalRecordFromStorage, 10);
      }
      // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    })
    this.sourcerowcriteria$.subscribe(data => {
      if (data) {
        let criteriaurl = data.ReportUri
        for(let i=0; i<criteriaurl.split('?')[1].split('&').length; i++){
          if(criteriaurl.split('?')[1].split('&')[i].includes('FirstNumber')){
            this.firstnumber = parseInt(criteriaurl.split('?')[1].split('&')[i].split('=')[1])
          }
          if(criteriaurl.split('?')[1].split('&')[i].includes('MaxResult')){
            this.maxresult = parseInt(criteriaurl.split('?')[1].split('&')[i].split('=')[1])
          }
        }
        this.MaxResult = "&MaxResult="+ this.maxresult;
        this.maxnum = this.maxresult
      }
    })
    this.listofview$.subscribe(res => {
      if (res) {
        this.viewtypes = [];
        for(let data of res){
          this.viewtypes.push(data)
        }
        this.viewtypes.unshift({ReportViewId:'', ReportViewType:'STD-HTML-VIEW' ,SecondTemplateLocation:'STDHTMLVIEW' ,ReportViewName:'STDHTMLVIEW',TemplateLocation:'STDHTMLVIEW'} as any)
        console.log("View Types:", this.viewtypes)
      }
    })
    this.visiblebuttons$.subscribe(res => {
      if (res) {
        this.viewmode = res;
      }
    })

    this.rowcerteria$.subscribe(res => {
      if (res) {
        this.menudetails = res;
        this.exceldata = res[0].ReportVsViewsArray;
      }
    })
  }

  prepreviousset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.firstnumber > 1){
      this.firstnumber = 1;
      this.maxresult = this.maxnum;
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          criteriaurl = criteriaurl.slice(0, -1);
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          console.log("criteria:",criteria)
          this.formaction.formactionservice(url,criteria).subscribe( (res:any) => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  previousset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.firstnumber > 1){
      if(this.firstnumber > this.maxnum){
        this.firstnumber -= this.maxnum;
        this.maxresult -= this.maxnum;
      }
      else {
        this.firstnumber = 1
      }
      if(this.maxresult % this.maxnum != 0){
        this.maxresult = Math.ceil(this.maxresult / this.maxnum) * this.maxnum;
      }
      
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          console.log("criteria:",criteria)
          this.formaction.formactionservice(url,criteria).subscribe( (res:any) => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  nextset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    console.log("TotalRecord:",this.TotalRecord,this.maxresult < this.TotalRecord || this.TotalRecord == 0)
    if(this.maxresult < this.TotalRecord || this.TotalRecord == 0){
      if(this.maxresult < this.maxnum){
        this.firstnumber += this.maxresult;
      }
      else{
        this.firstnumber += this.maxnum;
      }
      this.maxresult += this.maxnum;
      if(this.maxresult > this.TotalRecord && this.TotalRecord != 0){
        this.maxresult = this.TotalRecord
      }
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          criteriaurl = criteriaurl.slice(0, -1);
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          console.log("criteria:",criteria)
          this.formaction.formactionservice(url,criteria).subscribe( (res:any) => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  nextnextset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.maxresult < this.TotalRecord){
      this.maxresult = this.TotalRecord
      this.firstnumber = Math.floor(this.maxresult / this.maxnum) * this.maxnum + 1;
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          console.log("criteria:",criteria)
          this.formaction.formactionservice(url,criteria).subscribe( (res:any) => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }
 
  public selectedview(view:any) {
    this.store.dispatch(new SelectedViewtype(view))
  }

  public addRecord() {
    const add = this.formComponent.form as any;
    add.clear();
  }

  public path() {
    this.path$.subscribe(res => {
      if (res) {
        let testpath = res
        this.router.navigate([testpath]);
      }
    })
    this.store.dispatch(new ButtonVisibility("Forms"))
  }

   public Excel(data:any) {
  //   this.service.excelservice(data, this.menudetails, '').subscribe(res => {
  //     // console.log("Base645464", res);
  //     if (res) {
  //       let name;
  //       this.convertBase64ToExcel(res, name);
  //     }
  //   });
  // }
  // convertBase64ToExcel = function (bssix:any, fname:any) {
  //   var data = bssix;

  //   var contentType = 'application/vnd.ms-excel';
  //   // var blob1 = this.b64toBlobCommon(data, contentType);
  //   const blob1 = (this.b64toBlobCommon as (data: any, contentType: any) => any).bind(this)(data, contentType);
  //   var blobUrl1 = URL.createObjectURL(blob1);

  //   //window.open(blobUrl1);
  //   var downloadLink = document.createElement('a');
  //   downloadLink.href = blobUrl1;
  //   downloadLink.download = 'Report' + '.xlsx';
  //   document.body.appendChild(downloadLink);
  //   downloadLink.click();
  //   document.body.removeChild(downloadLink);
  // }.bind(this);;

  // public b64toBlobCommon = function (b64Data:any, contentType:any, sliceSize:any) {
  //   contentType = contentType || '';
  //   sliceSize = sliceSize || 512;

  //   var byteCharacters = atob(b64Data);
  //   var byteArrays = [];

  //   for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
  //     var slice = byteCharacters.slice(offset, offset + sliceSize);

  //     var byteNumbers = new Array(slice.length);
  //     for (var i = 0; i < slice.length; i++) {
  //       byteNumbers[i] = slice.charCodeAt(i);
  //     }

  //     var byteArray = new Uint8Array(byteNumbers);

  //     byteArrays.push(byteArray);
  //   }

  //   var blob = new Blob(byteArrays, { type: contentType });
  //   return blob;
   };



  public CSV(data:any) {
    this.service.excelservice(data, this.menudetails, '').subscribe(res => {
      // console.log("Base645464", res);
      if (res) {
        let name;
        // this.convertBase64ToCSV(res, name);
      }
    });
  }
  // convertBase64ToCSV = function (bssix:any, fname:any) {
  //   var data = bssix;

  //   var contentType = 'application/vnd.ms-excel';
  //   const blob1 = (this.b64toBlobCommon as (data: any, contentType: any) => any).bind(this)(data, contentType);
  //   // var blob1 = this.b64toBlobCommoncsv(data, contentType);
  //   var blobUrl1 = URL.createObjectURL(blob1);

  //   //window.open(blobUrl1);
  //   var downloadLink = document.createElement('a');
  //   downloadLink.href = blobUrl1;
  //   downloadLink.download = 'Report' + '.csv';
  //   document.body.appendChild(downloadLink);
  //   downloadLink.click();
  //   document.body.removeChild(downloadLink);
  // };

  b64toBlobCommoncsv = function (b64Data:any, contentType:any, sliceSize:any) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  };

  LoginDTO:any;
  ReportviewID:any;
  public pdf() { 
    let filterdata:any;
    this.sourcerowcriteria$.subscribe(data => {
      let finaldata = JSON.stringify(data.CriteriaDTO);
      filterdata = finaldata
      // console.log("filterdata", data)
    })
    let configurationjasperdetail = localStorage.getItem("JasperDetailUrl");
    this.LOGINDTO$.subscribe(dto => {
      // console.log("dto",dto);
      let Logindto = dto;
      this.LoginDTO = JSON.stringify(Logindto);
      // console.log("this.LoginDTO", this.LoginDTO)
    })
    this.selectedview$.subscribe(data => {
      // console.log("dataview", data)
      this.ReportviewID = data.Id
    })
    let DefaultReportFormatId:any;
    // this.reportformatselection$.subscribe(data => {
    //   // console.log("reportformatselection", data)
    //   DefaultReportFormatId = data;
    // })
    this.rowcerteria$.subscribe(res => {
      // console.log("JSJSJSJSJSJJ",res)
      if (res) {
        let btoafilter = btoa(filterdata as any);
        // console.log("btoafilter", btoafilter)
        let Reportname = res[0].ReportName
        let ReportFilename = res[0].ReportFile
        let weburl = res[0].WebServiceUriTemplate
        let re = /FirstNumber=1/gi;
        var newstr = weburl.replace(re, 'FirstNumber=-1');
        let x = /MaxResult=10/gi
        var Finalweburl = newstr.replace(x, 'MaxResult=-1');
        let symbol = /&/gi
        var Finalweburls = Finalweburl.replace(symbol, '@');
        let WebServiceUriTemplate = Finalweburls
        let MenuReportId = res[0].MenuReportId
        // console.log("Final", this.LoginDTO)
        // console.log("ReportFilename==", ReportFilename, "WebServiceUriTemplate==", WebServiceUriTemplate, "DefaultReportFormatId==", DefaultReportFormatId, "MenuReportId===", MenuReportId)
        let Base64LoginDTO = btoa(this.LoginDTO) // string to Base64 Converter (btoa) || (atob) to convert Base64 to string
        // console.log("Base64LoginDTO", Base64LoginDTO);
        let pdfdata = configurationjasperdetail + "/jasperserver/flow.html?_flowId=viewReportFlow&j_username=jasperadmin&j_password=jasperadmin&param=1&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=Freports&reportUnit=" + ReportFilename + "&output=pdf&Criteria=" + btoafilter + "&JasperReportTitle=" + Reportname + "&Method=POST&URL=" + WebServiceUriTemplate + "&Login=" + Base64LoginDTO + "&ReportFormatId=" + DefaultReportFormatId + "&ReportId=" + MenuReportId + "&ReportViewId=" + this.ReportviewID + "&IsDynamicJasperRequired=1";
        window.open(pdfdata, "_blank");
      }
      else {
        // const dialogRef = this.dialog.open(DialogBoxComponent, {
        //   data: {
        //     message: ['Please Select Any Report'],
        //     heading: 'Error',
        //     // button:'Ok'
        //     // Add any other properties you want to pass
        //   }
        // });
      }
    })
    // const add = this.formComponent as any;
    // var doc = new jsPDF('p', 'pt');

    // add.columnData.forEach(element => {
    //   var temp = element.headerName;
    //   this.cc.push(temp);
    // });

    // add.rowData.forEach(element => {
    //   var temp = [element.Code, element.Name];
    //   this.rr.push(temp);
    // });
  }

  

  public saveRecord() {
    console.log("this.formComponent:",this.formComponent)
    // const add = this.formComponent.form as any;
    // console.log("Save:",add)
    // add.save();
    const data = this.formComponent as any;
    // console.log("this.formComponent Db Service:",data.form)
    let url = data.form.ds.dbDataService.endPoint
      let criteria = data.form.value
      this.service.savedata(url, criteria).subscribe(res => {
        if (res.Body) {
          // const dialogRef = this.dialog.open(DialogBoxComponent, {
          //   data: {
          //     message: res.Body,
          //     heading: 'Success',
          //     // button:'Ok'
          //     // Add any other properties you want to pass
          //   }
          // });
          // alert(res.Body)
        }
        else {
        //   const dialogRef = this.dialog.open(DialogBoxComponent, {
        //   data: {
        //     message: res,
        //     heading: 'Error',
        //     // button:'Ok'
        //     // Add any other properties you want to pass
        //   }
        // });
      }
        data.form.clear();
      })
  }
  public editRecord(){
    const data = this.formComponent as any;
    this.edit$.pipe(take(1)).subscribe(newValue => {
      if (!newValue) {
        this.disableButton = false;
        this.store.dispatch(new FormEditable);
      } else {
        this.disableButton = true;
        this.store.dispatch(new FormUnEditable);
      }
    });
    let id: number = JSON.parse(data.Listrowselectedindex);
    this.movenextpreid = id
  }
  public clearRecord() {
    const add = this.formComponent.form as any;
    console.log("form", this.formComponent);
    add.clear();
  }
  public deleteRecord() {
    const add = this.formComponent.form as any;
    add.delete();
  }

  public movePrev() {
    const prev = this.formComponent.form as any;
    prev.movePrev();
  }
  public moveNext() {
    const next = this.formComponent.form as any;
    next.moveNext();
  }
  public moveFirst() {
    const first = this.formComponent.form as any;
    first.moveFirst();
  }

  public moveLast() {
    const Last = this.formComponent.form as any;
    Last.moveLast();
  }

  aboutclick() {
    // const dialogRef = this.dialog.open(GbaboutComponent, { position: { right: '0' } });
    // dialogRef.afterClosed().subscribe(result => {
    //   // console.log(`Dialog result: ${result}`);
    // });
    this.router.navigate(['/gbabout']);
  }
  gblogclick() {
    // const dialogRef = this.dialog.open(GblogComponent, { position: { right: '0' } });
    // dialogRef.afterClosed().subscribe(result => {
    //   // console.log(`Dialog result: ${result}`);
    // });
    this.router.navigate(['/gblog']);
  }
 
}



// Previous Code

// import { Component, Input, OnInit, isDevMode } from '@angular/core';
// import { GBBasePageComponentNG } from '@goodbooks/uicore';
// import { Observable } from 'rxjs';
// import { Select, Store } from '@ngxs/store';
// import { LayoutState } from './../../../../store/layout.state';
// import { ReportState } from './../../../../../commonreport/datastore/commonreport.state'
// import { FilterState } from './../../../../../gbfilter/store/gbfilter.state'
// import { AuthState } from './../../../../../common/shared/stores/auth.state'
// import { MenuserviceService } from './../../../../services/menu/menuservice.service'
// import { MatDialog } from '@angular/material/dialog';
// import * as utf8 from "utf8";
// import { Router } from '@angular/router';
// import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
// import { GbaboutComponent } from 'features/gbabout/components/gbabout/gbabout.component';
// import { FormEditOption, FormEditabe, FormUnEditable, Movenextpreviousid, ResetForm } from 'features/layout/store/layout.actions';
// import { SelectedViewtype } from 'features/commonreport/datastore/commonreport.action';
// import { take } from 'rxjs/operators';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
// const globalbase64js = require("base-64");
// const globalpako = require('pako');
// @Component({
//   selector: 'app-gbformactions',
//   templateUrl: './gbformactions.component.html',
//   styleUrls: ['./gbformactions.component.scss']
// })
// export class GbformactionsComponent implements OnInit {
//   @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
//   @Select(ReportState.SelectedViewtype) selectedview$: Observable<any>;
//   @Select(LayoutState.Visibilebuttons) visiblebuttons$: Observable<string>;
//   @Select(LayoutState.FormValidationvalues) formvaldiation$: Observable<string>;
//   @Select(ReportState.CriteriaConfigArray) rowcerteria$: Observable<any>;
//   @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
//   @Select(FilterState.Reportformatselection) reportformatselection$: Observable<any>;
//   @Select(FilterState.Filter) filtercriteria$: Observable<any>;
//   @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
//   @Select(LayoutState.Path) path$: Observable<any>;
//   @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
//   @Select(ReportState.ListofViewtypes) listofview$: Observable<any>;
//   @Select(LayoutState.Versionservicurl) jsaperdetails$: Observable<any>

//   @Input() public _formComponent: GBBasePageComponentNG;
//   @Input() public FormGroup: GBBasePageComponentNG;
//   public viewmode;
//   public exceldata;
//   public menudetails;
//   public movenextpreid
//   dummy = 1;
//   public disableButton = true;
//   rowData: any;
//   color;
//   public viewtypes = [];
//   public Validation;
//   public FormReset;
//   constructor(public store: Store, public dialog: MatDialog, private router: Router, public service: MenuserviceService) {
//   }

//   get formComponent(): GBBasePageComponentNG {
//     return this._formComponent;
//   }

//   get formgrp(): GBBasePageComponentNG {
//     return this.FormGroup;
//   }
//   @Input()
//   set formComponent(val: GBBasePageComponentNG) {
//     this._formComponent = val;
//   }
//   public cc = [];
//   public rr = [];

//   ngOnInit(): void {
//     this.store.dispatch(new FormUnEditable);
//     this.edit$.subscribe(res => {
//       this.disableButton = !res;
//     })
//     this.listofview$.subscribe(res => {
//       if (res) {
//         this.viewtypes = [];
//         for(let data of res){
//           this.viewtypes.push(data)
//         }
//         this.viewtypes.unshift({ReportViewId:'', ReportViewType:'STD-HTML-VIEW' ,SecondTemplateLocation:'STDHTMLVIEW' ,ReportViewName:'STDHTMLVIEW',TemplateLocation:'STDHTMLVIEW'})
//         console.log("View Types:", this.viewtypes)
//       }
//     })
//     this.visiblebuttons$.subscribe(res => {
//       if (res) {
//         this.viewmode = res;
//       }
//     })

//     this.rowcerteria$.subscribe(res => {
//       if (res) {
//         this.menudetails = res;
//         this.exceldata = res[0].ReportVsViewsArray;
//       }
//     })
//   }

//   public selectedview(view) {
//     this.store.dispatch(new SelectedViewtype(view))
//   }

//   public addRecord() {
//     const add = this.formComponent.form as any;
//     add.clear();
//   }

//   public path() {
//     this.path$.subscribe(res => {
//       if (res) {
//         console.log("PATH", res)
//         let testpath = res
//         this.router.navigate([testpath]);
//       }
//     })
//   }

//   public Excel(data) {
//     this.service.excelservice(data, this.menudetails, '').subscribe(res => {
//       console.log("Base645464", res);
//       if (res) {
//         let name;
//         this.convertBase64ToExcel(res, name);
//       }
//     });
//   }
//   convertBase64ToExcel = function (bssix, fname) {
//     var data = bssix;

//     var contentType = 'application/vnd.ms-excel';
//     var blob1 = this.b64toBlobCommon(data, contentType);
//     var blobUrl1 = URL.createObjectURL(blob1);

//     //window.open(blobUrl1);
//     var downloadLink = document.createElement('a');
//     downloadLink.href = blobUrl1;
//     downloadLink.download = 'Report' + '.xlsx';
//     document.body.appendChild(downloadLink);
//     downloadLink.click();
//     document.body.removeChild(downloadLink);
//   };

//   b64toBlobCommon = function (b64Data, contentType, sliceSize) {
//     contentType = contentType || '';
//     sliceSize = sliceSize || 512;

//     var byteCharacters = atob(b64Data);
//     var byteArrays = [];

//     for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       var slice = byteCharacters.slice(offset, offset + sliceSize);

//       var byteNumbers = new Array(slice.length);
//       for (var i = 0; i < slice.length; i++) {
//         byteNumbers[i] = slice.charCodeAt(i);
//       }

//       var byteArray = new Uint8Array(byteNumbers);

//       byteArrays.push(byteArray);
//     }

//     var blob = new Blob(byteArrays, { type: contentType });
//     return blob;
//   };



//   public CSV(data) {
//     this.service.excelservice(data, this.menudetails, '').subscribe(res => {
//       console.log("Base645464", res);
//       if (res) {
//         let name;
//         this.convertBase64ToCSV(res, name);
//       }
//     });
//   }
//   convertBase64ToCSV = function (bssix, fname) {
//     var data = bssix;

//     var contentType = 'application/vnd.ms-excel';
//     var blob1 = this.b64toBlobCommoncsv(data, contentType);
//     var blobUrl1 = URL.createObjectURL(blob1);

//     //window.open(blobUrl1);
//     var downloadLink = document.createElement('a');
//     downloadLink.href = blobUrl1;
//     downloadLink.download = 'Report' + '.csv';
//     document.body.appendChild(downloadLink);
//     downloadLink.click();
//     document.body.removeChild(downloadLink);
//   };

//   b64toBlobCommoncsv = function (b64Data, contentType, sliceSize) {
//     contentType = contentType || '';
//     sliceSize = sliceSize || 512;

//     var byteCharacters = atob(b64Data);
//     var byteArrays = [];

//     for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       var slice = byteCharacters.slice(offset, offset + sliceSize);

//       var byteNumbers = new Array(slice.length);
//       for (var i = 0; i < slice.length; i++) {
//         byteNumbers[i] = slice.charCodeAt(i);
//       }

//       var byteArray = new Uint8Array(byteNumbers);

//       byteArrays.push(byteArray);
//     }

//     var blob = new Blob(byteArrays, { type: contentType });
//     return blob;
//   };

//   LoginDTO;
//   ReportviewID;
//   public pdf() {
//     let filterdata;
//     this.sourcerowcriteria$.subscribe(data => {
//       let finaldata = JSON.stringify(data.CriteriaDTO);
//       filterdata = finaldata
//       console.log("filterdata", data)
//     })
//     let configurationjasperdetail = localStorage.getItem("JasperDetailUrl");
//     // this.jsaperdetails$.subscribe((data: any) => {
//     //   console.log("dataa", data.serverDetails.domain)
//     //   configurationjasperdetail = data.JasperDetails.Server;
//     // })
//     this.LOGINDTO$.subscribe(dto => {
//       let Logindto = dto;
//       this.LoginDTO = JSON.stringify(Logindto);
//       console.log("this.LoginDTO", this.LoginDTO)
//     })
//     this.selectedview$.subscribe(data => {
//       console.log("dataview", data)
//       this.ReportviewID = data.Id
//     })
//     let DefaultReportFormatId;
//     this.reportformatselection$.subscribe(data => {
//       console.log("reportformatselection", data)
//       DefaultReportFormatId = data;
//     })
//     this.rowcerteria$.subscribe(res => {
//       if (res) {
//         let btoafilter = btoa(filterdata);
//         console.log("btoafilter", btoafilter)
//         let Reportname = res[0].ReportName
//         let ReportFilename = res[0].ReportFile
//         let weburl = res[0].WebServiceUriTemplate
//         let re = /FirstNumber=1/gi;
//         var newstr = weburl.replace(re, 'FirstNumber=-1');
//         let x = /MaxResult=10/gi
//         var Finalweburl = newstr.replace(x, 'MaxResult=-1');
//         let symbol = /&/gi
//         var Finalweburls = Finalweburl.replace(symbol, '@');
//         let WebServiceUriTemplate = Finalweburls
//         let MenuReportId = res[0].MenuReportId
//         console.log("Final", this.LoginDTO)
//         console.log("ReportFilename==", ReportFilename, "WebServiceUriTemplate==", WebServiceUriTemplate, "DefaultReportFormatId==", DefaultReportFormatId, "MenuReportId===", MenuReportId)
//         let Base64LoginDTO = btoa(this.LoginDTO) // string to Base64 Converter (btoa) || (atob) to convert Base64 to string
//         console.log("Base64LoginDTO", Base64LoginDTO);
//         let pdfdata = configurationjasperdetail + "/jasperserver/flow.html?_flowId=viewReportFlow&j_username=jasperadmin&j_password=jasperadmin&param=1&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=Freports&reportUnit=" + ReportFilename + "&output=pdf&Criteria=" + btoafilter + "&JasperReportTitle=" + Reportname + "&Method=POST&URL=" + WebServiceUriTemplate + "&Login=" + Base64LoginDTO + "&ReportFormatId=" + DefaultReportFormatId + "&ReportId=" + MenuReportId + "&ReportViewId=" + this.ReportviewID + "&IsDynamicJasperRequired=1";
//         window.open(pdfdata, "_blank");
//       }
//       else {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: ['Please Select Any Report'],
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//       }
//     })
//     // const add = this.formComponent as any;
//     // var doc = new jsPDF('p', 'pt');

//     // add.columnData.forEach(element => {
//     //   var temp = element.headerName;
//     //   this.cc.push(temp);
//     // });

//     // add.rowData.forEach(element => {
//     //   var temp = [element.Code, element.Name];
//     //   this.rr.push(temp);
//     // });
//   }

//   public saveRecord() {
//     let flag = 1;
//     let data = this.formComponent as any
//     let conditioncheck = [null,undefined,'']
//     let validationkeys = Object.keys(this.Validation[0])
//     let alertdata = []
//     for(let formdata of Object.keys(data.form.value)){
//       if(validationkeys.includes(formdata)){
//         if(conditioncheck.includes(data.form.value[formdata])){
//           flag = 0;
//           alertdata.push(this.Validation[0][formdata]+" Should not be Empty")
//           // alert(this.Validation[0][formdata]+" Should not be Empty")
//         }
//       }
//     }
//     if(flag == 0){
//       const dialogRef = this.dialog.open(DialogBoxComponent, {
//         data: {
//           message: alertdata,
//           heading: 'Warning',
//           // button:'Ok'
//           // Add any other properties you want to pass
//         }
//       });
//     }
//     if (data.form.value && flag == 1) {
//       this.store.dispatch(new FormUnEditable);
//       let Value = data.form.value
//       let Keys = Object.keys(Value)
//       for (let i = 0; i < Keys.length; i++) {
//         if (typeof Value[Keys[i]] == 'object') {
//           let objectitem = ""
//           if (Value[Keys[i]] != null) {
//             for (let item of Value[Keys[i]]) {
//               objectitem = objectitem + item + ",";
//               data.form.value[Keys[i]] = objectitem;
//             }
//             data.form.value[Keys[i]] = data.form.value[Keys[i]].slice(0, -1);
//           }
//         }
//       }
//       let url = data.service.dbDataService.endPoint
//       let criteria = data.form.value
//       this.service.savedata(url, criteria).subscribe(res => {
//         if (res.Body) {
//           const dialogRef = this.dialog.open(DialogBoxComponent, {
//             data: {
//               message: res.Body,
//               heading: 'Success',
//               // button:'Ok'
//               // Add any other properties you want to pass
//             }
//           });
//           // alert(res.Body)
//         }
//         else {const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: res,
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//           // alert(res)
//         }
//         data.form.reset(this.FormReset)

//       })
//     }
//   }
//   public clearRecord() {
//     const data = this.formComponent as any;
//     data.form.reset(this.FormReset)
//   }
//   public editRecord() {
//     this.formvaldiation$.subscribe(res => {
//       this.Validation = res[0]
//       this.Validation = this.Validation.Validation
//       this.FormReset = res[1]
//       this.FormReset = this.FormReset.Reset
//       console.log("Validation:",this.Validation)
//       console.log("FormReset:",this.FormReset)
//     })
//     const data = this.formComponent as any;
//     this.edit$.pipe(take(1)).subscribe(newValue => {
//       if (!newValue) {
//         this.disableButton = false;
//         this.store.dispatch(new FormEditabe);
//       } else {
//         this.disableButton = true;
//         this.store.dispatch(new FormUnEditable);
//       }
//     });
//     let id: number = JSON.parse(data.Listrowselectedindex);
//     this.movenextpreid = id

//   }
//   public deleteRecord() {
//     const data = this.formComponent as any;
//     let url = data.service.dbDataService.endPoint
//     let idfied = data.service.dbDataService.idField
//     let selectedid = data.service.dbDataService.selectid
//     this.service.deletedata(url, idfied, selectedid).subscribe(res => {
//       console.log("Delete Response:",res)
//       console.log("isDevMode():",isDevMode())
//       let delresponse;
//       if(isDevMode()){
//         delresponse = this.responsehandler(res)
//       } else {
//         delresponse = this.responsehandler(res.contents)
//       }
      
//       console.log("Delete delresponse:",delresponse)
//       if (delresponse.Body) {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: delresponse.Body,
//             heading: 'Success',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//         // alert(delresponse.Body)
//       }
//       else {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: delresponse,
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//         // alert(delresponse)
//       }

//       data.form.reset(this.FormReset)
//     })
//   }
//   responsehandler(encodeddata: any) {
//     const getresponse = JSON.stringify(encodeddata);
//     const responseModel = JSON.parse(getresponse);
//     let apiBody = responseModel.Body;
//     const bytes = utf8.decode(apiBody);
//     const decodedData = globalbase64js.decode(bytes);
//     const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
//     const con = Responsebodydecoded.Body;
//     let converter = Responsebodydecoded;
//     try {
//       converter = JSON.parse(con)
//     } catch (error) {
//       converter = con
//     }
//     return converter;
//   }
//   public movePrev() {
//     this.movenextpreid = this.movenextpreid - 1
//     this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
//     console.log("this.movenextpreid", this.movenextpreid)
//   }
//   public moveNext() {
//     this.movenextpreid = this.movenextpreid + 1
//     this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
//     console.log("this.movenextpreid", this.movenextpreid)
//   }
//   public moveFirst() {
//     const first = this.formComponent.form as any;
//     first.moveFirst();
//   }

//   public moveLast() {
//     const Last = this.formComponent.form as any;
//     Last.moveLast();
//   }

//   aboutclick() {
//     const dialogRef = this.dialog.open(GbaboutComponent, { position: { right: '0' } });
//     dialogRef.afterClosed().subscribe(result => {
//       console.log(`Dialog result: ${result}`);
//     });
//     this.router.navigate(['/gbabout']);
//   }
//   gblogclick() {
//     const dialogRef = this.dialog.open(GblogComponent, { position: { right: '0' } });
//     dialogRef.afterClosed().subscribe(result => {
//       console.log(`Dialog result: ${result}`);
//     });
//     this.router.navigate(['/gblog']);
//   }

// }
