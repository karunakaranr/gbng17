import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { GbmenutreeComponent } from './gbmenutree.component';
import { MenuitemComponent } from './../gbmenuitem/gbmenuitem.component';

import { MatMenuModule } from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { BrowserModule } from '@angular/platform-browser';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
const routes: Routes = [
    {
        path: 'gbmenutree',
        component: GbmenutreeComponent
    },
    {
        path: 'app-menu-item',
        component: MenuitemComponent
    }

];

@NgModule({
    declarations: [

        GbmenutreeComponent,
        MenuitemComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),

        MatMenuModule,
        MatExpansionModule,
        MatTreeModule,
        MatIconModule,
        MatButtonModule,
        BrowserModule

    ],
    exports: [
        RouterModule,
        GbmenutreeComponent,
        MenuitemComponent,
        MatMenuModule,
        MatExpansionModule,

    ]


})
export class GBMenutreeModule { }
