import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbappsidebarComponent } from './gbappsidebar.component';

describe('GbappsidebarComponent', () => {
  let component: GbappsidebarComponent;
  let fixture: ComponentFixture<GbappsidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbappsidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbappsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
