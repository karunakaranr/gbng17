import { Component } from '@angular/core';
import { SidebarService } from './../../gbappsidebar/sidebar.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ILoginDTO } from './../.../../../../../common/shared/models/Login/Logindto';
import { AuthState } from './../../../../common/shared/stores/auth.state';
import { CloseMenuBar, OpenMenuBar } from '../../../store/layout.actions';
import { take } from 'rxjs/operators';
import { LayoutState } from '../../../store/layout.state';

@Component({
  selector: 'app-gbhbmenu',
  templateUrl: './gbhbmenu.component.html',
  styleUrls: ['./gbhbmenu.component.scss']
})
export class GbhbmenuComponent  {
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<ILoginDTO>;
  OuName!:string;
  constructor(public sidenav:SidebarService , public store: Store) { }

  ngOnInit(): void {
    this.loginDTOs$.subscribe(dto => {
      this.OuName = dto.OuCode;
    })
  }
  @Select(LayoutState.MenuBarOpened) leftsidebarOpen$!: Observable<boolean>;
  toggleMenu() {
    this.leftsidebarOpen$.pipe(take(1)).subscribe(newValue => {
      if(!newValue) this.store.dispatch(new OpenMenuBar); else this.store.dispatch(new CloseMenuBar);
    });
  }

}
