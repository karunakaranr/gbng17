import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbnotificationComponent } from './gbnotification.component';

describe('GbnotificationComponent', () => {
  let component: GbnotificationComponent;
  let fixture: ComponentFixture<GbnotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbnotificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbnotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
