import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbSettingsComponent } from './gbsettings.component';

describe('GbSettingsComponent', () => {
  let component: GbSettingsComponent;
  let fixture: ComponentFixture<GbSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
