import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { GBHttpService } from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { Router } from '@angular/router';
import { StateClear } from 'ngxs-reset-plugin';
import { Select, Store } from '@ngxs/store'
import { Observable, Subscription } from 'rxjs';
import { AuthState } from './../../../../../common/shared/stores/auth.state';
import { ILoginDTO } from './../.../../../../../../common/shared/models/Login/Logindto';
import {MatDialog} from '@angular/material/dialog';
import {GbSettingsComponent } from './../../gbsettings/gbsettings.component';
import { ChangeTheme, ChangeMode } from './../../../../../../features/layout/store/layout.actions';
@Component({
  selector: 'app-gbusersetting',
  templateUrl: './gbusersetting.component.html',
  styleUrls: ['./gbusersetting.component.scss']
})
export class GbusersettingComponent implements OnInit {
  public DB!: string;
  public user!: string;
  public change!: null;
  public usercolor!:any;
  public color ='Black';
  public imgTraceSrc='assets/UserSetting/Black/Trace.png';
  public imgThemeSrc='assets/UserSetting/Black/Theme.png';
  public imgSettingSrc='assets/UserSetting/Black/Setting.png';
  public imgLoggedInSrc='assets/UserSetting/Black/Logged.png';
  public imgAboutSrc='assets/UserSetting/Black/About.png';
  public imgLogoutSrc='assets/UserSetting/Black/Logout.png';
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<ILoginDTO | null>
  @Select(AuthState.GetTheme) theme$!: Observable<any>;
  private formSubscription: Subscription = new Subscription();
  constructor(@Inject(GBHttpService) public HTTP: GBHttpService, public store: Store, public route: Router , public dialog: MatDialog, private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.formSubscription.add(
      this.loginDTOs$.subscribe(dto => {
        if (dto !== null) {
          this.user = dto.UserName;
        }
      })
    );
  }


  public logout() {
    const UserName = this.user;
    const url = '/fws/User.svc/LogOut/?UserCode=' + UserName;
    this.HTTP.httppost(url, '').subscribe((Res:any) => {
      if (Res) {
        this.store.dispatch(new StateClear());
        this.store.dispatch(new ChangeTheme('Default', this.ref));
        this.store.dispatch(new ChangeMode('light', this.ref));
      }
    });
  }
  openDialog() {
    const dialogRef = this.dialog.open(GbSettingsComponent , {position: { right: '0'}} );
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  changeimageTraceWhite(){
    this.imgTraceSrc='assets/UserSetting/White/Trace.png';
    
  }

  changeimageTraceBlack(){
    this.imgTraceSrc='assets/UserSetting/Black/Trace.png';
    
  }

  changeimageThemeWhite(){
    this.imgThemeSrc='assets/UserSetting/White/Theme.png';
    
  }

  changeimageThemeBlack(){
    this.imgThemeSrc='assets/UserSetting/Black/Theme.png';
    
  }

  changeimageSettingWhite(){
    this.imgSettingSrc='assets/UserSetting/White/Setting.png';
    
  }

  changeimageSettingBlack(){
    this.imgSettingSrc='assets/UserSetting/Black/Setting.png';
    
  }

  changeimageLoggedInWhite(){
    this.imgLoggedInSrc='assets/UserSetting/White/Logged.png';
    
  }

  changeimageLoggedInBlack(){
    this.imgLoggedInSrc='assets/UserSetting/Black/Logged.png';
    
  }

  changeimageAboutWhite(){
    this.imgAboutSrc='assets/UserSetting/White/About.png';
    
  }

  changeimageAboutBlack(){
    this.imgAboutSrc='assets/UserSetting/Black/About.png';
    
  }

  changeimageLogoutWhite(){
    this.imgLogoutSrc='assets/UserSetting/White/Logout.png';
    
  }

  changeimageLogoutBlack(){
    this.imgLogoutSrc='assets/UserSetting/Black/Logout.png';
    
  }

}
