import { Component, OnInit, VERSION ,ViewChild } from '@angular/core';
import { GbformtitleComponent} from './../gbmaincontent/gbformheader/gbformtitle/gbformtitle.component';
@Component({
  selector: 'app-gbappfooter',
  templateUrl: './gbappfooter.component.html',
  styleUrls: ['./gbappfooter.component.scss']
})
export class GBAppFooterComponent implements OnInit {
  @ViewChild(GbformtitleComponent) title!: GbformtitleComponent;
  ngOnInit(): void {
  }

  get angularVersion() { return VERSION.full; }

}
