export class IBizTransaction {
	BizTransactionId?: number;
	BizTransactionCode?: string;
	BizTransactionName?: string;
	BizTransactionTypeId?: number;
	BizTransactionTypeCode?: string;
	BizTransactionTypeName?: string;
	BizTransactionSubClassId?: number;
	BizTransactionSubClassCode?: string;
	BizTransactionSubClassName?: string;
	BizTransactionClassId?: number;
	BizTransactionClassCode?: string;
	BizTransactionClassName?: string;
	RoleId?: number;
	RoleName?: string;
	MenuId?: number;
	MenuName?: string;
	Allow?: number;
	View?: number | string;
	Insert?: number | string;
	Update?: number | string;
	Delete?: number | string;
	Print?: number | string;
	Export?: number | string;
	BackDateEntryDay?: string;
	BackDateUpdateDay?: string;
	BackDateDeleteDay?: string;
	ForwardEntryDate?: string;
	OverAllLockDate?: string;
	BackDateCheckType?: number;
	LockOpenCloseStatus?: number;
	LockOpenCloseDate?: string;
	EntityId?: number;
	URIParameterValue?: string;
}