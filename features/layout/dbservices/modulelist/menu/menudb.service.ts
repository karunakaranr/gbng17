import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { IMenulist } from './../../../models/Imenu.model';
import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { GbUIURLS } from './../../../URLS/urls';
import { HttpClient, HttpParams } from '@angular/common/http';
// import {FilterState} from './../../../../gbfilter/store/gbfilter.state'
import {AuthState} from './../../../../common/shared/stores/auth.state';
@Injectable({
  providedIn: 'root'
})
export class MenudbService {

  constructor(private http: GBHttpService, private httpp: HttpClient) { }
  // @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(AuthState.AddDTO) LOGINDTO$!: Observable<any>;
  public MenulistService(ModuleId:string) {
    let userid;
    let userIdParam = userid !== undefined ? userid : '';
    let moduleIdParam = ModuleId !== undefined ? ModuleId : '';
    this.LOGINDTO$.subscribe(dto =>{
      let Logindto = dto;
      userid = dto.UserId;
    })
    const param = new HttpParams({fromObject: {UserId:userIdParam,ModuleId: moduleIdParam}});
    let params = {
      UserId: userid,
      ModuleId: ModuleId
    }
    return this.http.httpgetparams(GbUIURLS.MENULIST, params)
    // const testurl = '/fws/Menu.svc/UserModuleMenuTree/?UserId=-1499998941&ModuleId=-1899999989'
   // return this.http.httpget(GbUIURLS.MENULIST+ModuleId);
  //  return this.http.httpget(testurl);

  }


  public exceldbservice(exceldata:any , menudata:any , data:any) {
    let criterias:any; 
    // this.filtercriteria$.subscribe(data => {
    //   if(data){
    //     if(data != "Empty"){
    //       criterias =  data
    //     }
    //   }
    // })
    let weburl = menudata[0].WebServiceUriTemplate
    let re = /FirstNumber=1/gi;
    var newstr = weburl.replace(re, 'FirstNumber=-1');
    let x = /MaxResult=10/gi
    var Finalweburl = newstr.replace(x, 'MaxResult=-1');

    let criteria = {
      "ReportUri": Finalweburl,
      "Method": menudata[0].MethodType,
      "ReportTitle": exceldata.ReportViewName,
      "CriteriaDTO": criterias,
      "EntityId": menudata[0].EntityId,
      "IsExcelImport": 2,
      "ReportFormatId": exceldata.TemplateId,
      "ReportId": exceldata.TemplateId,
      "ReportViewId": exceldata.ReportViewId
    }
    let url;
    console.log("excelcritera", criteria)
    let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
    let str = fullurl.split('/gb4');
    let urlstr = str[str.length - 1];
    url = urlstr;
    console.log("api", url);
    return this.http.httppost(url, criteria);
  }


  public savedata(url:any,criteria:any){
    console.log("URL",url,"criteria",criteria)
    return this.http.httppost(url, criteria);
  }

  public deletedata(url:any,idfied:any, selectedid:any){
    let finalurl = url + idfied + selectedid
    return this.http.httpdelete(finalurl)
  }
}





