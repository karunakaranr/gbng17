import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { GBHttpService } from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { GbUIURLS } from './../../URLS/urls';
import { IModulelist } from './../../models/Imodule.model';
@Injectable({
  providedIn: 'root',
})
export class Moduledbservice {
  constructor(private http: GBHttpService) { }
  public ModulelisetService(): Observable<IModulelist> {
    return this.http.httpget(GbUIURLS.MODULELIST);

  }

}
