import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoBcComponent } from './lo-bc.component';

describe('LoBcComponent', () => {
  let component: LoBcComponent;
  let fixture: ComponentFixture<LoBcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoBcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoBcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
