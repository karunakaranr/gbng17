import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetThemeListDirective, GetThemeModeListDirective } from './directives/theme';
import { RouterModule, Routes } from '@angular/router';
import { BsformComponent } from './testScreens/bsform/bsform.component';
import { MatformComponent } from './testScreens/matform/matform.component';
import { HtmlformComponent } from './testScreens/htmlform/htmlform.component';
import { ThemeTestComponent } from './themetest.component';
import { DemoMaterialModule } from './testScreens/demo.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  // {
  //   path: '',
  //   component: ThemeTestComponent,
    
  // },
  {
    path: 'matform',
    component: MatformComponent
  },
  {
    path: 'htmlform',
    component: HtmlformComponent
  },
  {
    path: 'bsform',
    component: BsformComponent
  },
]


@NgModule({
  declarations: [
    GetThemeListDirective,
    GetThemeModeListDirective,

    ThemeTestComponent, BsformComponent, MatformComponent, HtmlformComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DemoMaterialModule
  ],
  exports: [
    GetThemeListDirective,
    GetThemeModeListDirective,

    ThemeTestComponent, BsformComponent, MatformComponent, HtmlformComponent,
  ]
})
export class ThemesModule { }
