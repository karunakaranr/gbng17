export class IDashboard {
    UserVsPortletId: Number;
    UserId: Number;
    UserCode: String;
    UserName: String;
    PageId: string;
    PageCode: any;
    PageName: string;
    PortletId: Number;
    PortletCode: String;
    PortletName: string;
    CriteriaConfigId: Number;
    CriteriaConfigName: string;
    UserVsPortletHeight: Number;
    UserVsPortletWidth: Number;
    UserVsPortletSlNo: Number;
    UserVsPortletPortletRow: Number;
    UserVsPortletPortletColumn: Number;
    XPosition: String;
    YPosition: string;
    Component: String;
    TypeId: String;
}



