export class GridSetting{
    headerName! : string;
    field! : string;
    sortable! : boolean;
    filter! : boolean;
    checkboxSelection! : boolean;
    resizable! : boolean;
    editable! : boolean;
    pinned! : string;
    cellEditor! : string;
    cellEditorParams! : string;
}
export class GridProperties{
    paginationPageSize! : number;
    pagination! : boolean;
    undoRedoCellEditing! : boolean;
    undoRedoCellEditingLimit! : number;
    enableCellChangeFlash! : boolean;
    enterMovesDown! : boolean;
    enterMovesDownAfterEdit! : boolean;
    rowSelection! : string;
    enableRangeSelection! : boolean;
    paginateChildRows! : boolean;
    routingpath!: string;
    routingparams!: string;
    routingfieldvalue!: string;
}

export class Commonreportcol{
    MenuReportId!: number;
    ReportViewFieldsId!: number;
    ReportViewId!: number;
    ReportViewName!: string;
    ReportVsFieldsId!: number;
    ReportVsFieldsSlNo!: number;
    ReportVsFieldsFieldName!: string;
    ReportVsFieldsFieldTitle!: string;
    ReportVsFieldsFieldType!: number;
    ReportVsFieldsFieldWidth!: number;
    ReportVsFieldsIsDisplay!: number;
    ReportVsFieldsIsMergeColumn!: number;
    ReportVsFieldsIsGroupColumn!: number;
    ReportVsFieldsIsSubtotalColumn!: number;
    ReportVsFieldsIsGrandtotalColumn!: number;
    ReportVsFieldsCalFormat!: string;
    ReportVsFieldsDisplaySlNo!: number;
    CriteriaAttributeId!: number;
}