import { Injectable } from '@angular/core';
import {PortletdbService} from './../../dbservices/portlet/portletDB.service';

@Injectable({
  providedIn: 'root'
})
export class PortletService {
  
constructor(public portletDB: PortletdbService ){}

public getportlet()
{
return this.portletDB.ViewPortlet();
}
}
