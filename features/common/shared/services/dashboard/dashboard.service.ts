import { Injectable } from '@angular/core';
import {  IDashboard } from './../../models/dashboard/dashboard.model';
import { DashboarddbService} from './../../dbservices/dashboard/dashboard.dbservice';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  PageViewmodal : IDashboard = new IDashboard();

constructor(public dashboardDB: DashboarddbService ){}

public getuserpage()
{
return this.dashboardDB.Viewuserpage();
}

public saveuserpage(savedata:IDashboard)
{
return this.dashboardDB.saveuserpage(savedata);
}

public removepages(event,item)
{
  return this.dashboardDB.removepages(event,item);
}
}
