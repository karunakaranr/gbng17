import { Injectable } from '@angular/core';
import { GBBaseService } from './../../../../../libs/gbdata/src/lib/gbdata.module';



import { ILoginDetail } from './../../models/Login/LoginDetail';
import { Authkeyservice } from './Authkey.service';


@Injectable({
  providedIn: 'root',
})
export class Loginservice extends GBBaseService {

  constructor(public Authkey: Authkeyservice) {
    super();
   }

  public LoginService(userdetails: ILoginDetail,uri:any) {
    return this.Authkey.Authkeyservice(userdetails,uri);
  }
}
