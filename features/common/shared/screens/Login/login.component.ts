import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GBBasePageComponentNG, GBBasePageComponent} from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { GBDataPageServiceNG} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import {GBFormGroup,GBBaseFormGroup } from './../../../../../libs/uicore/src/lib//classes/GBFormGroup';
import { ILoginDetail } from './../../models/Login/LoginDetail';
import { Versionservice } from './../../services/Login/Version.service';
import {AuthkeyDBservice} from './../../dbservices/Login/AuthkeyDB.service';
import { Select } from '@ngxs/store';
import { LayoutState } from './../../../../../features/layout/store/layout.state';
// import * as jsonvalue from './../../../../../apps/Goodbooks/Goodbooks/src/assets/Carousel_Image/Image.json';
// import * as themecolors from './../../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/ThemeColor.json';

@Component({
  selector: 'app-logincommon',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG},
    {provide: 'DataService', useClass: Versionservice}
  ]
})
export class LoginComponent extends GBBasePageComponentNG {
  @Select(LayoutState.CurrentTheme) currenttheme: any;   
  form!: GBBaseFormGroup
  currentIndex = 0;
  slides: string | any;
  title = 'login';
  label = 'Login';
  componentName = 'user'; 
  hide = true;
  async thisConstructor() {
    let jsonvalue:any;
    // this.gbps.gbhttp.httpget('/assets/Carousel_Image/Image.json').subscribe((res:any)=>{
    //   console.log("LLLL",res);
    //   jsonvalue = res;
    // })
    this.form = this.gbps.fb.group({
      Server: ['GB4SC', Validators.required],
      UserName: ['ADMIN', Validators.required],
      Password: ['admin', Validators.required]
    }) as unknown as GBBaseFormGroup;
    // this.currenttheme.subscribe( (theme) => {
    //     this.setStyle('--color', themecolors[theme]);
    // })
    // let abc=[];
    // for(let data of jsonvalue){
    //   abc.push({image: `assets/Carousel_Image/Images/${data.name}`})
    // }
    // this.slides=abc;
    // while (true) { 
      
    //   await this.delay(10000); 
    //   this.prevSlide()
    // }
  }
  delay(milliseconds: number) { 
    return new Promise(resolve => setTimeout(resolve, milliseconds)); 
  }

  setStyle(name:any,value:any) {
    document.documentElement.style.setProperty(name, value);
  } 
  
  setCurrentSlideIndex(index:any) {
    this.currentIndex = index;
  }

  isCurrentSlideIndex(index:any) {
    return this.currentIndex === index;
  }

  prevSlide() {
    this.currentIndex = (this.currentIndex < this.slides.length - 1 ) ? ++this.currentIndex : 0;
  }

  nextSlide() {
    this.currentIndex = (this.currentIndex > 0) ? --this.currentIndex : this.slides.length - 1;
  }

  public Login() {
    const logindata: ILoginDetail = this.form.value as ILoginDetail;
    (this.gbps.dataService as Versionservice).getversioninfo(logindata); 
    console.log("logindata :",logindata)
    console.log("this.gbps.dataService :",this.gbps.dataService)
  }

  public clear() {
    this.form.reset();
  }
}

