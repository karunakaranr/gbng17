import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


// import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';

import { TranslocoRootModule } from './../../../../transloco/transloco-root.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
// import { TranslateModule } from './../../Translate/Components/translate.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDividerModule} from '@angular/material/divider';
// import { GBLayoutModule } from 'features/layout/gblayout.module';
import {UifwkUifwkmaterialModule} from './../../../../../libs/uifwk/uifwkmaterial/src/lib/uifwk-uifwkmaterial.module'
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule } from '../../../../../apps/Goodbooks/Goodbooks/src/app/sharedscreens/Shared/Translate/Components/translate.module';
@NgModule({
  declarations: [
    LoginComponent,
    
  ],
  imports: [
    NgbModule,
    // GBLayoutModule,
    CommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    RouterModule.forChild(LoginRoutingModule),
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule
  ],
  exports: [
    LoginComponent
  ],
  bootstrap: [
    LoginComponent
  ],
})
export class LoginModule { }
