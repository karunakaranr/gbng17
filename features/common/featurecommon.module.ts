import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { LoginModule } from './shared/screens/Login/login.module';
import { GbcommonModule } from './../../libs/gbcommon/src/lib/gbcommon.module';
import { TranslateModule } from './../../features/common/shared/Translate/translate.module';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { AuthState } from './shared/stores/auth.state';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDividerModule} from '@angular/material/divider';
// import { DashboardModule } from './shared/screens/dashboard/dashboard.module';
// import { PortletModule } from './shared/screens/portlet/portlet.module';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbgridModule } from './../../features/gbgrid/components/gbgrid/gbgrid.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    GbcommonModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    LoginModule,
    GbgridModule,
    // DashboardModule,
    // PortletModule,
    NgxsModule.forFeature([AuthState]),
    // NgxChartsModule
  ],
  // exports: [LoginModule,GbgridModule,DashboardModule],
  exports: [LoginModule,GbgridModule]
})
export class FeatureCommonmodule { }
