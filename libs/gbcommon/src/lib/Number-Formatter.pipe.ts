import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from 'features/common/shared/stores/auth.state';
@Pipe({
  name: 'NumberFormat'
})
export class NumberFormatPipe implements PipeTransform {
  loginDTOs$: Observable<any>
  numberformat!: string;
  constructor(private store: Store){
    this.loginDTOs$ = this.store.select(AuthState.AddDTO);
  }
  transform(value: number, group: string): string {
    this.loginDTOs$.subscribe(dto => {
      this.numberformat = dto.CurrencyFormat.split('.')[1].length
    })
    if (value === 0) {
      if(group){
        return '0.00';
      }else{
        return '';
      }
    } else {
      const formattedValue = value < 0 ? -value : value;
      return formattedValue.toLocaleString('en-IN', {
        maximumFractionDigits: parseInt(this.numberformat),
        minimumFractionDigits: parseInt(this.numberformat)
      });
    }
  }
}
