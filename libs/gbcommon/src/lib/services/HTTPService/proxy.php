<?PHP


// Script: Simple PHP Proxy: Get external HTML, JSON and more!
//
// *Version: 1.6, Last updated: 1/24/2009*
// 
// Project Home - http://benalman.com/projects/php-simple-proxy/
// GitHub       - http://github.com/cowboy/php-simple-proxy/
// Source       - http://github.com/cowboy/php-simple-proxy/raw/master/ba-simple-proxy.php
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrates one way
// in which this PHP script can be used.
// 
// Simple - http://benalman.com/code/projects/php-simple-proxy/examples/simple/
// 
// About: Release History
// 
// 1.6 - (1/24/2009) Now defaults to JSON mode, which can now be changed to
//       native mode by specifying ?mode=native. Native and JSONP modes are
//       disabled by default because of possible XSS vulnerability issues, but
//       are configurable in the PHP script along with a url validation regex.
// 1.5 - (12/27/2009) Initial release
// 
// Topic: GET Parameters
// 
// Certain GET (query string) parameters may be passed into ba-simple-proxy.php
// to control its behavior, this is a list of these parameters. 
// 
//   url - The remote URL resource to fetch. Any GET parameters to be passed
//     through to the remote URL resource must be urlencoded in this parameter.
//   mode - If mode=native, the response will be sent using the same content
//     type and headers that the remote URL resource returned. If omitted, the
//     response will be JSON (or JSONP). <Native requests> and <JSONP requests>
//     are disabled by default, see <Configuration Options> for more information.
//   callback - If specified, the response JSON will be wrapped in this named
//     function call. This parameter and <JSONP requests> are disabled by
//     default, see <Configuration Options> for more information.
//   user_agent - This value will be sent to the remote URL request as the
//     `User-Agent:` HTTP request header. If omitted, the browser user agent
//     will be passed through.
//   send_cookies - If send_cookies=1, all cookies will be forwarded through to
//     the remote URL request.
//   send_session - If send_session=1 and send_cookies=1, the SID cookie will be
//     forwarded through to the remote URL request.
//   full_headers - If a JSON request and full_headers=1, the JSON response will
//     contain detailed header information.
//   full_status - If a JSON request and full_status=1, the JSON response will
//     contain detailed cURL status information, otherwise it will just contain
//     the `http_code` property.
// 
// Topic: POST Parameters
// 
// All POST parameters are automatically passed through to the remote URL
// request.
// 
// Topic: JSON requests
// 
// This request will return the contents of the specified url in JSON format.
// 
// Request:
// 
// > ba-simple-proxy.php?url=http://example.com/
// 
// Response:
// 
// > { "contents": "<html>...</html>", "headers": {...}, "status": {...} }
// 
// JSON object properties:
// 
//   contents - (String) The contents of the remote URL resource.
//   headers - (Object) A hash of HTTP headers returned by the remote URL
//     resource.
//   status - (Object) A hash of status codes returned by cURL.
// 
// Topic: JSONP requests
// 
// This request will return the contents of the specified url in JSONP format
// (but only if $enable_jsonp is enabled in the PHP script).
// 
// Request:
// 
// > ba-simple-proxy.php?url=http://example.com/&callback=foo
// 
// Response:
// 
// > foo({ "contents": "<html>...</html>", "headers": {...}, "status": {...} })
// 
// JSON object properties:
// 
//   contents - (String) The contents of the remote URL resource.
//   headers - (Object) A hash of HTTP headers returned by the remote URL
//     resource.
//   status - (Object) A hash of status codes returned by cURL.
// 
// Topic: Native requests
// 
// This request will return the contents of the specified url in the format it
// was received in, including the same content-type and other headers (but only
// if $enable_native is enabled in the PHP script).
// 
// Request:
// 
// > ba-simple-proxy.php?url=http://example.com/&mode=native
// 
// Response:
// 
// > <html>...</html>
// 
// Topic: Notes
// 
// * Assumes magic_quotes_gpc = Off in php.ini
// 
// Topic: Configuration Options
// 
// These variables can be manually edited in the PHP file if necessary.
// 
//   $enable_jsonp - Only enable <JSONP requests> if you really need to. If you
//     install this script on the same server as the page you're calling it
//     from, plain JSON will work. Defaults to false.
//   $enable_native - You can enable <Native requests>, but you should only do
//     this if you also whitelist specific URLs using $valid_url_regex, to avoid
//     possible XSS vulnerabilities. Defaults to false.
//   $valid_url_regex - This regex is matched against the url parameter to
//     ensure that it is valid. This setting only needs to be used if either
//     $enable_jsonp or $enable_native are enabled. Defaults to '/.*/' which
//     validates all URLs.
// 
// ############################################################################

// Change these configuration options if needed, see above descriptions for info.
$enable_jsonp    = false;
$enable_native   = true;
$valid_url_regex = '/.*/';

//Regular expression to allow request only inside youtility.in
//$valid_url_regex = '#^https?://([a-z0-9-]+\.)*youtility\.in(/.*)?$#';


//Allowed IP address to access this proxy
 
$allowedHosts = array(
    "localhost",
	"192.168.1.36",
	"127.0.0.1",
  "::1"
    
);


 $parsedDomain ="";
 
   if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $parsedDomain=$_SERVER['HTTP_CLIENT_IP'];

    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $parsedDomain=$_SERVER['HTTP_X_FORWARDED_FOR'];
	 
    }
    else
    {
      $parsedDomain=$_SERVER['REMOTE_ADDR'];
    }
	
	if((!(validateIpAddress($parsedDomain))) && $_SERVER['HTTP_HOST'] == 'localhost')
	{
	 $parsedDomain = "::1";
	}
	
 
if (in_array($parsedDomain, $allowedHosts))
{
    $validHost = true;
} 
else
{
    $validHost = true;
}
 

// ############################################################################

$url = $_GET['url'];

if ( !$url ) {
  
  // Passed url not specified.
  $contents = 'ERROR: url not specified';
  $status = array( 'http_code' => 'ERROR' );
  
}
else if ($validHost == false)
{
    $contents = 'ERROR: invalid referrer';
    $status   = array(
        'http_code' => 'ERROR'
    );
   
} //$validHost == false

 else if ( !preg_match( $valid_url_regex, $url ) ) {
  
  // Passed url doesn't match $valid_url_regex.
  $contents = 'ERROR: invalid url';
  $status = array( 'http_code' => 'ERROR' );
  
} else {
  $ch = curl_init( $url );
  
  if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' ) 
  {
    
    curl_setopt( $ch, CURLOPT_POST, true );
    
	//curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode(file_get_contents("php://input")) );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, file_get_contents("php://input"));
	
  }
  elseif (strtolower($_SERVER['REQUEST_METHOD']) == 'delete' ){
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");  
  
  }
  
  //if ( $_GET['send_cookies'] ) 
  {
    $cookie = array();
    foreach ( $_COOKIE as $key => $value ) {
      $cookie[] = $key . '=' . $value;
    }
    if ( isset($_GET['send_session']) && $_GET['send_session'] ) 
	{
     // $cookie[] = SID;
	  
	//  print implode( '; ', $cookie );
    }
    $cookie = implode( '; ', $cookie );
    
	
	
    curl_setopt( $ch, CURLOPT_COOKIE, $cookie );
  } 
  
  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
  curl_setopt( $ch, CURLOPT_HEADER, true );
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

  
    // curl_setopt( $ch, CURLINFO_HEADER_OUT, true );

//print ($parsedDomain);
  
  curl_setopt($ch, CURLOPT_HTTPHEADER, getHeadersFromServer($parsedDomain,false) );   //true vv changed to false  
  
  //print_r($ch);
  
  
 // curl_setopt( $ch, CURLOPT_USERAGENT, $_GET['user_agent'] ? $_GET['user_agent'] : $_SERVER['HTTP_USER_AGENT'] );
  
  list( $header, $contents ) = preg_split( '/([\r\n][\r\n])\\1/', curl_exec( $ch ), 2 );
  
  $status = curl_getinfo( $ch );
  
  curl_close( $ch );
}

// Split header text into an array.
$header_text = preg_split( '/[\r\n]+/', $header );


if ( isset($_GET['mode']) && $_GET['mode'] == 'native' ) {
  if ( !$enable_native ) {
    $contents = 'ERROR: invalid mode';
    $status = array( 'http_code' => 'ERROR' );
  }
  
  // Propagate headers to response.
  foreach ( $header_text as $header ) {
    if ( preg_match( '/^(?:Content-Type|Content-Language|Set-Cookie):/i', $header ) ) {
      header( $header );
    }
  }
  
  print $contents;
  
} else {
  
  // $data will be serialized into JSON data.
  $data = array();
  
  // Propagate all HTTP headers into the JSON data object.
  if ( isset($_GET['full_headers']) && $_GET['full_headers'] ) {
    $data['headers'] = array();
    
    foreach ( $header_text as $header ) {
      preg_match( '/^(.+?):\s+(.*)$/', $header, $matches );
      if ( $matches ) {
        $data['headers'][ $matches[1] ] = $matches[2];
      }
    }
  } 
  
  // Propagate all cURL request / response info to the JSON data object.
  if ( isset($_GET['full_status']) && $_GET['full_status'] ) {
    $data['status'] = $status;
  } else {
    $data['status'] = array();
    $data['status']['http_code'] = $status['http_code'];
  }
  
  // Set the JSON data object contents, decoding it from JSON if possible.
  $decoded_json = json_decode( $contents );
  $data['contents'] = $decoded_json ? $decoded_json : $contents;
  
  // Generate appropriate content-type header.
  $is_xhr = strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
  header( 'Content-type: application/' . ( $is_xhr ? 'json' : 'x-javascript' ) );
  
  // Get JSONP callback.
  $jsonp_callback = $enable_jsonp && isset($_GET['callback']) ? $_GET['callback'] : null;
  
  // Generate JSON/JSONP string
  $json = json_encode( $data );
  
  print $jsonp_callback ? "$jsonp_callback($json)" : $json;
  
}

function validateIpAddress($ip_addr)
{
  //first of all the format of the ip address is matched
  if(preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/",$ip_addr))
  {
    //now all the intger values are separated
    $parts=explode(".",$ip_addr);
    //now we need to check each part can range from 0-255
    foreach($parts as $ip_parts)
    {
      if(intval($ip_parts)>255 || intval($ip_parts)<0)
      return false; //if number is not within range of 0-255
    }
    return true;
  }
  else
    return false; //if format of ip address doesn't matches
}

function getHeadersFromServer($reqDomain, $onlyCookie=false)
{
    $headers = array();
	if($onlyCookie){
        $headers[] = 'Cookie: '.preg_replace('/'.PREFIX_COOKIE.'(.+=)?/', '$1', $_SERVER['HTTP_COOKIE']);
		
	
		
	}else{
	
	 //print_r($_SERVER);
	
		 
	    foreach ($_SERVER as $k => $v)
	    {		
			if ((substr($k, 0, 5) == "HTTP_" || substr($k, 0, 5) == "Login" ) && substr($k, 0, 9) != "HTTP_HOST")
		    {
			
			 if(substr($k, 0, 9) == "HTTP_AUTH") {
			  $headers[] = 'Authorization: '.str_replace('"','',$v);
			  continue;
			 } 
			 
		        $k = str_replace('_', ' ', substr($k, 5));
		        $k = str_replace(' ', '-', ucwords(strtolower($k)));
				
				
				 
				if($k=='Cookie')
				{
				
				$headers[] = 'Cookie: '. $v;
			
        			//$headers[] = 'Cookie: '.preg_replace('@'.PREFIX_COOKIE.'(.+=)?@', '${1}', $v);
					}
				else
		        	$headers[] = $k.': '.$v;
		    }
			
			 if(substr($k, 0, 12) == "CONTENT_TYPE") {
			  $k = str_replace('_', '-', ucwords(strtolower($k)));
			  $headers[] = $k.': '.$v;
			 }
			 
			 
			 
			 
			 
			 
			  
			  
			  
			 
			 
			 
			
		}
    }
	
		
	$headers[] = 'IP: '.$reqDomain;
	//$headers[] = 'Authorization: Basic YWRtaW46dW5pc29mdA==';
	
	
	
	
	
//print_r($headers);

//print_r($_SERVER);


    return $headers;
}

?>
