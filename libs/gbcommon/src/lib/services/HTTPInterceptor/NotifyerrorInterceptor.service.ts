import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GbToasterService } from './../toaster/gbtoaster.service';

@Injectable({
  providedIn: 'root',
})
export class NotifyErrorService implements HttpInterceptor {
  constructor(public toaster:GbToasterService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.toaster.showerror('Password Wrong...!Try Again','Error');
            return throwError(err);
          } else {
            const error = err.error || err.statusText;
            this.toaster.showerror(error,'Error');
            return throwError(error);
          }
        }
        else {
          const errormsg = err || err.statusText;
          this.toaster.showerror(err,'Error');
        }
        return throwError('An error occurred');
      })
    );
  }
}
