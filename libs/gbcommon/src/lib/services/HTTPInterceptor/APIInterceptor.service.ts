import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpHeaders
} from '@angular/common/http';
import { Inject, Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APIResponse, ProxyResponse, ResponseModel } from './../../model/interceptormodel.model'
import { Store } from '@ngxs/store';

// import utf8 from 'utf8';
declare let require: any;
const globalbase64js = require("base-64");
const globalpako = require('pako');

@Injectable({
  providedIn: 'root'
})
export class APIinterceptor implements HttpInterceptor {
  constructor(@Inject('env') private env:any, private store: Store) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const desktop = false; //this.deviceService.isDesktop();
    let URL = request.url;//.replace('//','/');
    console.log("intercepr url" + URL)
    const apireq = request.clone({
      headers: request.headers.set(
        'Login', this.getLoginDTO() // this.loginheader,
      ),
      url: URL
    });
    const apiurl = this.env.gbsettings.apiurl; //data$.apibaseurl;
    let proxlocation = this.env.gbsettings.proxylocation; //data$.apibaseurl;

    const apiurlencryptprefix = 'api';
    const apiurluncryptprefix = 'apiu';
    if (request.url.startsWith('assets')) {
      return next.handle(request);
    }
    else {
      // if (URL.indexOf(apiurlencryptprefix) >= 0) {
      /* URL = URL.replace(apiurlencryptprefix, '').replace('//', '/');
 
       if (URL.indexOf(apiurluncryptprefix) >= 0)
         URL = URL.replace(apiurluncryptprefix, '').replace('//', '/');
       if (URL.indexOf(apiurlencryptprefix) >= 0)
         URL = URL.replace(apiurlencryptprefix, '').replace('//', '/');
      
       if (proxlocation == undefined) {
         proxlocation = ""
       }
       URL = proxlocation + apiurl + URL;
       URL = URL.replace('prox//', 'prox/');*/
      // if (desktop && isDevMode()) {
      //   URL = 'prox' + URL;
      // }
      // else {
      //   URL = proxyurl + apibaseurl + URL;
      // }

      //test//if(request.url.startsWith('.sjson')){}

      return next.handle(apireq).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            try {

              const getresponse = JSON.stringify(event);
              const responseModel: ResponseModel = JSON.parse(getresponse);

              let apiResp: APIResponse = responseModel.body as APIResponse;
              const proxResp: ProxyResponse = responseModel.body as ProxyResponse;
              if (proxResp.contents) apiResp = proxResp.contents;
              if (!apiResp) return event;

              let apiBody = apiResp.Body;
              // const bytes = utf8.decode(apiBody);
              const bytes = apiBody;
              const decodedData = globalbase64js.decode(bytes);
              const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));

              const con = Responsebodydecoded.Body;
              let converter = Responsebodydecoded;

              if (event.status === 200 && apiResp) {
                if (apiResp.Status != '200') {
                  throw new HttpErrorResponse({
                    error: converter.Body,
                    headers: event.headers,
                    status: event.body.Status,
                    statusText: 'Warning',
                  });

                }
                /* try {
                                  converter = event.clone({ body: JSON.parse(Responsebodydecoded) })
                                } catch (error) {
                                  converter = event.clone({ body: Responsebodydecoded })
                */

                try {
                  converter = event.clone({ body: JSON.parse(con) })
                } catch (error) {
                  converter = event.clone({ body: con })

                }
              }
              return converter;
            }
            finally {
              //Nothing
            }
          }
        }),
      );
    }

  }

  public getLoginDTO(): string {
    let retvalue = '';
    this.store.selectSnapshot((state) => {
      if (state.AuthStore?.DLoginDTO != null)
        retvalue = JSON.stringify(state.AuthStore.DLoginDTO);
      if (state.AppStore?.DLoginDTO != null)
        retvalue = JSON.stringify(state.AppStore.DLoginDTO);

    });
    return retvalue;
  }

}
