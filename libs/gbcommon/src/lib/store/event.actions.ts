import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Events';

export class Notify {
  static readonly type = '[' + storeSliceName + '] Notify';
  constructor(public msg: string, public data: unknown = null) {  }
}

export class Event {
  static readonly type = '[' + storeSliceName + '] Event';
  constructor(public channel: string, public evtdata: unknown = null) {  }
}
