import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { GBUIMetadataService } from '../services/gbui.metadata.service';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[accessControl]',
})
export class AccessControlDirective implements OnInit {
  @Input() moduleType: string;
  @Input() accessType: string;
  constructor(private elementRef: ElementRef, private auth: GBUIMetadataService) {}

  ngOnInit() {
    this.elementRef.nativeElement.style.display = 'none';
    this.checkAccess();
  }
   checkAccess() {
    const accessControls: any = this.auth.getAccessControls();
    const module: any = accessControls.find(access => access.module_name === this.moduleType);
    this.elementRef.nativeElement.style.display = module[this.accessType] ? 'block' : 'none';
  }
}
