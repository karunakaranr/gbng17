import { TestBed, waitForAsync } from '@angular/core/testing';
import { UicoreModule } from './uicore.module';

describe('UicoreModule', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [UicoreModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(UicoreModule).toBeDefined();
  });
});
