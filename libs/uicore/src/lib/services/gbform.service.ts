import { Injectable } from '@angular/core';
import { GBHttpService } from './../../../../gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IScreen } from '../interfaces/ibasefield';

@Injectable()
export class GBFormService {
  endPoint!: string;
  constructor(protected http: GBHttpService) {
  }

  getByID(id: string): Observable<IScreen> {
    return this.http.httpget('/assets/mockdata/_general/screens.json')
      .pipe(
        map(res => {
          return (res as IScreen[]).filter(f => f.ID === id)[0];
        })
      );
  }

  getByName(name: string): Observable<IScreen> {
    return this.http.httpget('/assets/mockdata/_general/screens.json')
      .pipe(
        map(res => {
          return (res as IScreen[]).filter(f => f.Name === name)[0];
        })
      );
  }

  getformdata(id: string): Observable<IScreen> {
    return this.http.httpget('/assets/mockdata/_general/screens.json')
    .pipe(
      map(res => {
        return (res as IScreen[]).filter(f => f.ID === id)[0];
      })
    );
  }
}
