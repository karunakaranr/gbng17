import { Inject, Injectable } from '@angular/core';
import { inject } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
// import { GBHttpService} from '@goodbooks/gbcommon';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN,GBBaseService } from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
// import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService, GBBaseService } from '@goodbooks/gbdata';
import { Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})
export class GBPageService {
  constructor(public store: Store, public fb: FormBuilder, public gbhttp:GBHttpService,
    public activeroute:ActivatedRoute,public router:Router) {

  }
}

@Injectable({
  providedIn: 'root'
})
export class GBDataPageServiceNG extends GBPageService {
  constructor(public override store: Store, @Inject('DataService') public dataService: GBBaseService, public override fb: FormBuilder, public override gbhttp:GBHttpService,
  public override activeroute:ActivatedRoute,public override router:Router) {
      super(store, fb, gbhttp, activeroute, router);
  }
}
@Injectable({
  providedIn: 'root'
})
export class GBDataPageService<T> extends GBPageService {
  constructor(public override store: Store, @Inject('DataService') public dataService: GBBaseDataService<T>, @Inject('DBDataService') public dbDataService: GBBaseDBDataService<T>, public override fb: FormBuilder, public override gbhttp:GBHttpService,
  public override activeroute:ActivatedRoute,public override router:Router) {
      super(store, fb, gbhttp, activeroute, router);
  }
}

@Injectable({
  providedIn: 'root'
})
export class GBDataPageServiceWN<T> extends GBDataPageService<T> {
  constructor(public override store: Store, @Inject('DataService') public override dataService: GBBaseDataServiceWN<T>, @Inject('DBDataService') public override dbDataService: GBBaseDBDataService<T>, public override fb: FormBuilder, public override gbhttp:GBHttpService,
  public override activeroute:ActivatedRoute,public override router:Router) {
      super(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
  }
}

@Injectable({
  providedIn: 'root'
})
export class GBDataPageServiceStore<T> extends GBPageService {
    constructor(public override store: Store, public override fb: FormBuilder, public override gbhttp:GBHttpService,
      public override activeroute:ActivatedRoute,public override router:Router) {
      super(store, fb, gbhttp, activeroute, router);
  }
}

@Injectable({
  providedIn: 'root'
})
export class GBDataPageServiceStoreWN<T> extends GBDataPageServiceStore<T> {
    constructor(public override store: Store, public override fb: FormBuilder, public override gbhttp:GBHttpService,
      public override activeroute:ActivatedRoute,public override router:Router) {
      super(store, fb, gbhttp, activeroute, router);
  }
}
