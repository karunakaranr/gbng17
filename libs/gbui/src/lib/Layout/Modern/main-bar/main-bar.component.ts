import { Component} from '@angular/core';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { GBHttpService } from './../../../../../../gbcommon/src/lib/services/HTTPService/GBHttp.service';

@Component({
  selector: 'app-main-bar',
  templateUrl: './main-bar.component.html',
  styleUrls: ['./main-bar.component.scss']
})
export class MainBarComponent  {

  constructor(public http: GBHttpService) { }
}
