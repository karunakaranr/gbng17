import { Component, OnInit } from '@angular/core';
import { GBHttpService } from './../../../../../../../gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { Router } from '@angular/router';
import { StateClear } from 'ngxs-reset-plugin';
import { Select, Store } from '@ngxs/store'
import { Observable, Subscription } from 'rxjs';
import { AuthState } from './../../../../../../../../features/common/shared/stores/auth.state';
import { ILoginDTO } from './../.../../../../../../../../../features/common/shared/stores/auth.model';


@Component({
  selector: 'app-settingmenu',
  templateUrl: './settingmenu.component.html',
  styleUrls: ['./settingmenu.component.scss']
})
export class settingmenuComponent implements OnInit {
  public DB: string ='';
  public user: string ='';
  public change: null = null;
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<ILoginDTO>;
  @Select(AuthState.GetTheme) theme$!: Observable<any>;
  private formSubscription: Subscription = new Subscription();
  constructor(public HTTP: GBHttpService, public store: Store, public route: Router) { }
  

  ngOnInit(): void {
    this.formSubscription.add(
      this.loginDTOs$.subscribe(dto => {
        this.user = dto.UserName;
      })
    );
  }
  public logout() {
    const UserName = this.user;
    const url = '/prox/fws/User.svc/LogOut/?UserCode=' + UserName;
    this.HTTP.httppost(url, '').subscribe((Res:any) => {
      if (Res) {
        this.store.dispatch(new StateClear());
      }
    });
  }

}
