import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GBsearchComponent } from './gbsearch.component';

describe('HeaderComponent', () => {
  let component: GBsearchComponent;
  let fixture: ComponentFixture<GBsearchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GBsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GBsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
