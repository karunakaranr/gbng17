import { Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { GBBasePageComponentNG } from './../../../../../uicore/src/lib/components/Base/GBBasePage';

@Component({
  selector: 'app-formheader',
  templateUrl: './formheader.component.html',
  styleUrls: ['./formheader.component.scss']
})
export class FormHeaderComponent implements OnInit {
  formtitle: string ='';
  @Input() private _formComponent!: GBBasePageComponentNG;

  constructor() {
   }

   get formComponent(): GBBasePageComponentNG {
     return this._formComponent;
   }
   @Input()
   set formComponent(val: GBBasePageComponentNG) {
     this._formComponent = val;
     this.formtitle = this._formComponent.title;
   }


  ngOnInit(): void {
    
}

public addRecord() {
  const add = this.formComponent.form as any;
  add.save();
}

public clearRecord() {
  const add = this.formComponent.form as any;
  add.clear();
}
public deleteRecord() {
  const add = this.formComponent.form as any;
  add.delete();
}

public movePrev() {
  const prev = this.formComponent.form as any;
  prev.movePrev();
}
public moveNext() {
  const next = this.formComponent.form as any;
  next.moveNext();
}
public moveFirst() {
  const first = this.formComponent.form as any;
  first.moveFirst();
}

public moveLast() {
  const Last = this.formComponent.form as any;
  Last.moveLast();
}

}
