import { Component, OnInit, ViewChild} from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { GBBasePageComponentNG, GBBasePageComponent } from './../../../../../uicore/src/lib/components/Base/GBBasePage';
import { TitleBarComponent } from './../Modern/title-bar/title-bar.component';


@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss']
})
export class ScreenComponent implements OnInit {
  @ViewChild(RouterOutlet) page!: RouterOutlet;
  @ViewChild(TitleBarComponent) title!: TitleBarComponent;
  constructor() { }


  ngOnInit(): void {
  }


  onActivate(){
    if (this.page){
      if (this.page.component){
      this.title.formComponent = this.page.component as GBBasePageComponentNG;
      }
    }
  }
}
