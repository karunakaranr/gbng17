import { AbstractControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { IActionSetLock, IActionSetRequired, IActionSetValue, IActionSetVisible, IActionSetError, IActionResult, IRuleContext, ICondition } from '../interfaces/RulesModel';

export function SetLock(context: IRuleContext, options: unknown | IActionSetLock): IActionResult {
  const opt = options as IActionSetLock;
  const retValue: IActionResult = {IsValid: true, Message: ''};
  if (context.data instanceof FormGroup) {
    const fg: FormGroup = context.data as FormGroup;
    const fc: AbstractControl = fg.controls[opt.Field];
    if (opt.Locked)
      fc.disable();
    else
      fc.enable();
    return {IsValid: true, Message: ''};
  }
  else {
    const data: any = context.data;
    if (opt.Locked && data[opt.Field])
      return {IsValid: false, Message: 'Field ' + opt.Field + ' is locked, cannot be set'};
    else
      return {IsValid: true, Message: ''};
  }
}

export function SetRequired(context: IRuleContext, options: unknown | IActionSetRequired): IActionResult {
  const opt = options as IActionSetRequired;
  if (context.data instanceof FormGroup) {
    const fg: FormGroup = context.data as FormGroup;
    const fc: AbstractControl = fg.controls[opt.Field];

    let newValidFns: ValidatorFn[] = [];
    if (Array.isArray(fc.validator)) {
      newValidFns = newValidFns.concat(fc.validator);
    }
    newValidFns = newValidFns.filter(obj => obj !== Validators.required);

    if (opt.Required)
      newValidFns.push(Validators.required);

    fc.clearValidators();
    fc.setValidators(newValidFns);
    fc.updateValueAndValidity();
    return {IsValid: true, Message: 'Field ' + opt.Field + ' is required'};
  }
  else {
    const data: any = context.data;
    if (opt.Required && !data[opt.Field])
      return {IsValid: false, Message: 'Field ' + opt.Field + ' is required'};
    else
      return {IsValid: true, Message: ''};
  }
}

export function SetValue(context: IRuleContext, options: unknown | IActionSetValue): IActionResult { //, formulaFunction: cbCalcFormulaFn, callFunction: cbCallFunctionFn): void {
  const opt = options as IActionSetValue;
  const newVal = context.engine.getValue(opt.ValueType, opt.Value);
  if (context.data instanceof FormGroup) {
    const fg: FormGroup = context.data as FormGroup;
    const fc: AbstractControl = fg.controls[opt.Field];
    fc.setValue(newVal, { onlySelf: true });
    return {IsValid: true, Message: ''};
  }
  else {
    const data: any = context.data;
    data[opt.Field] = newVal;
    return {IsValid: true, Message: ''};
  }
}

export function SetVisible(context: IRuleContext, options: unknown | IActionSetVisible): IActionResult {
  throw "SetVisible: Not implemented!";
  // const opt = options as IActionSetVisible;
  // if (context instanceof FormGroup) {
  //   const fg: FormGroup = context as FormGroup;
  //   const fc: AbstractControl = fg.controls[opt.Field];
  //   if (opt.Visible)
  //     fc.
  //   else
  //     fc.enable();
  // }
  // else
  //   throw "Unknown context!";
}

export function SetError(context: IRuleContext, options: unknown | IActionSetError): IActionResult {
  const opt = options as IActionSetError;
  if (context.data instanceof FormGroup) {
    const fg: FormGroup = context.data as FormGroup;
    const fc: AbstractControl = fg.controls[opt.Field];
    if (opt.ErrorName)
    {
      const err = {[opt.ErrorName]: true, errMsg: opt.ErrorMessage};
      if (opt.Set) {
        fc.setErrors(err);
        return {IsValid: false, Message: opt.ErrorMessage};
      }
      else
        return {IsValid: true, Message: ''};
    }
  }
  else {
    const data: any = context.data;
    if (opt.Set) {
      return {IsValid: false, Message: opt.ErrorMessage};
    }
    else
      return {IsValid: true, Message: ''};
  }
  return {IsValid: true, Message: ''};
}

export function ActOnCondition(context: IRuleContext, options: unknown | ICondition): IActionResult {
  const opt = options as ICondition;
  context.engine.checkAndRunCondition(opt);
  return {IsValid: true, Message: ''};
}

