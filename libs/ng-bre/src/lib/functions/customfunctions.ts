import { IRuleContext } from "../interfaces/RulesModel";

export function today(context: IRuleContext) {
  return Date.now();
}
export function negToday(context: IRuleContext) {
  return -Date.now();
}
export function getDayInSecs(context: IRuleContext) {
  return (1000 * 60 * 60 * 24);
}
export function newgGetDayInSecs(context: IRuleContext) {
  return -(1000 * 60 * 60 * 24);
}
