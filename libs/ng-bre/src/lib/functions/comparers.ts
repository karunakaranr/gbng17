import { IRuleContext } from "../interfaces/RulesModel";

export function AreEqual(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return factValue === compareValue;
}

export function AreNotEqual(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return factValue !== compareValue;
}

export function IsLessThan(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "number" && typeof compareValue === "number") {
    const numValue: number = factValue as number;
    const comValue: number = compareValue as number;
    return numValue < comValue;
  }
  else if (factValue instanceof Date && compareValue instanceof Date) {
    const dtmValue: Date = factValue as Date;
    const comValue: Date = compareValue as Date;
    return dtmValue < comValue;
  }
  else
    return false;
}

export function IsLessThanOrEqual(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "number" && typeof compareValue === "number") {
    const numValue: number = factValue as number;
    const comValue: number = compareValue as number;
    return numValue <= comValue;
  }
  else if (factValue instanceof Date && compareValue instanceof Date) {
    const dtmValue: Date = factValue as Date;
    const comValue: Date = compareValue as Date;
    return dtmValue <= comValue;
  }
  else
    return false;
}

export function IsGreaterThan(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "number" && typeof compareValue === "number") {
    const numValue: number = factValue as number;
    const comValue: number = compareValue as number;
    return numValue > comValue;
  }
  else if (factValue instanceof Date && compareValue instanceof Date) {
    const dtmValue: Date = factValue as Date;
    const comValue: Date = compareValue as Date;
    return dtmValue > comValue;
  }
  else
    return false;
}

export function IsGreaterThanEqual(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "number" && typeof compareValue === "number") {
    const numValue: number = factValue as number;
    const comValue: number = compareValue as number;
    return numValue >= comValue;
  }
  else if (factValue instanceof Date && compareValue instanceof Date) {
    const dtmValue: Date = factValue as Date;
    const comValue: Date = compareValue as Date;
    return dtmValue >= comValue;
  }
  else
    return false;
}

export function ContainsData(context: IRuleContext, factValue: unknown): boolean {
  return (factValue ? true : false);
}

export function DoesNotContainData(context: IRuleContext, factValue: unknown): boolean {
  return !ContainsData(context, factValue);
}

export function BeginsWith(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "string" && typeof compareValue === "string") {
    const strValue: string = factValue as string;
    const comValue: string = compareValue as string;
    return strValue.startsWith(comValue);
  }
  else
    return false;
}

export function DoesNotBeginWith(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return !BeginsWith(context, factValue, compareValue);
}

export function EndsWith(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "string" && typeof compareValue === "string") {
    const strValue: string = factValue as string;
    const comValue: string = compareValue as string;
    return strValue.endsWith(comValue);
  }
  else
    return false;
}

export function DoesNotEndWith(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return !EndsWith(context, factValue, compareValue);
}

export function Contains(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  if (typeof factValue === "string" && typeof compareValue === "string") {
    const strValue: string = factValue as string;
    const comValue: string = compareValue as string;
    return strValue.indexOf(comValue) >= 0;
  }
  else
    return false;
}

export function DoesNotContain(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return !Contains(context, factValue, compareValue);
}

export function NotApplicable(context: IRuleContext, factValue: unknown, compareValue: unknown): boolean {
  return factValue !== compareValue;
}
