import { EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscriber } from 'rxjs';
import { RuleEngine } from '../core/rule-engine';

export interface IRule {
  ID?: string;
  Name?: string;
  Condition: ICondition;
}

export interface IRuleContext {
  data: FormGroup | unknown;
  engine: RuleEngine;
  obs: Subscriber<IActionResult>;
  currentRule: IRule;
  currentAction: IAction;
  currentCondition: ICondition;
}

export interface IActionResult {
  IsValid: boolean;
  Message: string;
  Rule?: IRule;
  Action?: IAction;
  Context?: unknown;
}

export interface ICondition {
  Fact: string;
  Comparer: EnumComparers;
  ValueType?: EnumValueType;
  Value?: unknown;
  Truthy: IAction[] | ICondition;
  Falsy?: IAction[] | ICondition;
}

export interface IAction {
  Name: string;
}

export interface IActionSetError extends IAction {
  Field: string;
  Set: boolean;
  ErrorName: string;
  ErrorMessage: string;
}

export interface IActionSetVisible extends IAction {
  Field: string;
  Visible: boolean;
}

export interface IActionSetValue extends IAction {
  Field: string;
  ValueType: EnumValueType;
  Value: unknown | IFormula;
}

export interface IActionSetRequired extends IAction {
  Field: string;
  Required: boolean;
}

export interface IActionSetLock extends IAction {
  Field: string;
  Locked: boolean;
}

export interface IActionCheckCondition extends IAction {
  Field: string;
  Condition: ICondition;
}

export type FormulaFn = (value: unknown) => unknown;
// export type FormulaCtrlFn = (ctrl: FormControl) => unknown;
export type FormulaFormFn = (form: FormGroup) => unknown;
export interface IFormula {
  ResultFn: FormulaFn | FormulaFormFn;
  Field: string;
  Operator: EnumOperators | string;
  ValueType: EnumValueType;
  Value: unknown | IFormula;
}

export enum EnumComparers {
  Changed = -1,
  NotApplicable = 0,
  AreEqual = 1,
  AreNotEqual = 2,
  IsLessThan = 3,
  IsLessThanOrEqual  = 4,
  IsGreaterThan = 5,
  IsGreaterThanEqual = 6,
  ContainsData = 7,
  DoesNotContainData = 8,
  BeginsWith = 9,
  DoesNotBeginWith = "DoesNotBeginWith",
  EndsWith = 11,
  DoesNotEndWith = 12,
  Contains = 13,
  DoesNotContain = 14,
}

export enum  EnumOperators {
  Add = 0,
  Subtract = 1,
  Multiply = 2,
  Divide = 3
}

export enum EnumValueType {
  Constant = 0,
  Field = 1,
  Formula = 2,
  Function = 3,
  Clear = 4
}
