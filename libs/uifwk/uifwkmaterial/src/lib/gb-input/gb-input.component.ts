import { Component, ElementRef, Input, ViewChild } from '@angular/core';

import {GBBaseUIFCComponent} from './../../../../../../libs/uicore/src/lib/components/Base/GBBaseUIFCComponent'
@Component({
  selector: 'app-gb-input',
  templateUrl: './gb-input.component.html',
  styleUrls: ['./gb-input.component.scss']
})
export class GbInputComponent extends GBBaseUIFCComponent {
  @Input() minLen!: number;
  @Input() maxLen!: number;
  @Input() floatLabel: 'always' | 'never' | 'auto' = 'always';
  @Input() appearance: 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
  @Input() HtmlType = 'text';
  @Input() HtmlPattern = '';
  @ViewChild('input') input!: ElementRef;
  
  // public ReadOnly = true;
  onKeyDown(e: any): boolean {
    if ([8, 9, 27, 13, 46].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return true;
    }
    return false;
  }
 
}
