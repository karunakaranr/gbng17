import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { GbInputComponent } from './gb-input.component';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GbInputComponent],
  imports: [CommonModule, RouterModule, MatInputModule, ReactiveFormsModule],
  exports: [GbInputComponent],
})
export class GbInputModule {}
