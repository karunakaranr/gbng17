import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GbMatBaseComponent } from './gb--base/gb--base.component';
import { GbAddressModule } from './gb-address/gb-address.module';
import { GbCheckboxModule } from './gb-checkbox/gb-checkbox.module';
import { GbCurrencyModule } from './gb-currency/gb-currency.module';
import { GbDateModule } from './gb-date/gb-date.module';
import { GbEmailModule } from './gb-email/gb-email.module';
import { GbEntitylookupModule } from './gb-entitylookup/gb-entitylookup.module';
import { GbInputModule } from './gb-input/gb-input.module';
import { GbMobileModule } from './gb-mobile/gb-mobile.module';
import { GbNumericModule } from './gb-numeric/gb-numeric.module';
import { GbPhoneModule } from './gb-phone/gb-phone.module';
import { GbRadioboxModule } from './gb-radiobox/gb-radionbox.module';
import { GbTextareaModule } from './gb-textarea/gb-textarea.module';
import { GbComboboxModule } from './gb-combobox/gb-combobox.module';
// import { GBBaseComponent } from '@goodbooks/uicore';
import { GbDateTimeModule } from './gb-datetime/gb-datetime.module';

@NgModule({
  imports: [CommonModule],
  exports: [GbAddressModule,
  GbInputModule,
  GbNumericModule,
  GbDateModule,
  GbEmailModule,
  GbMobileModule,
  GbPhoneModule,
  GbRadioboxModule,
  GbTextareaModule,
  GbCheckboxModule,
  GbEntitylookupModule,
  GbCurrencyModule,
  GbComboboxModule,
  // GbDateTimeModule
],
declarations:[GbMatBaseComponent]
})
export class UifwkUifwkmaterialModule {}
