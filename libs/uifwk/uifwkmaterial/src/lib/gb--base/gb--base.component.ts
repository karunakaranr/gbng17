import { Component, OnInit, Input, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';

// Yet to be implemented; this is for generalizing all input components to have a wrapper around it...

@Component({
  selector: 'app-gb--base',
  template: './gb--base.component.html'
})
export class GbMatBaseComponent extends GBBaseUIFCComponent {
  @ViewChild('fieldComponent', { read: ViewContainerRef, static: true }) fieldComponent!: ViewContainerRef;
}
