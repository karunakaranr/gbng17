import { Component, OnInit } from '@angular/core';

import { GbInputComponent } from '../gb-input/gb-input.component';

@Component({
  selector: 'app-gb-email',
  templateUrl: './gb-email.component.html',
  styleUrls: ['./gb-email.component.scss']
})
export class GbEmailComponent extends GbInputComponent implements OnInit{
  private regex = '';
  // ngOnInit() {
  //   this.regex = this.Field.ValidRegEx;
  //   if (this.regex === ''){
  //   this.Field.ValidRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  //   }
  // }
}
