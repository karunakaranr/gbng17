import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule} from '@angular/forms';
import { GbEmailComponent } from './gb-email.component';
import { GbInputModule } from './../gb-input/gb-input.module';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    GbEmailComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    GbInputModule,
    MatFormFieldModule],
  exports: [
    GbEmailComponent
  ],
})
export class GbEmailModule {}
