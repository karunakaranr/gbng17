import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule} from '@angular/forms';
import { GbCurrencyComponent } from './gb-currency.component';
import { GbNumericModule } from './../gb-numeric/gb-numeric.module';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    GbCurrencyComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    GbNumericModule,
    MatFormFieldModule],
  exports: [
    GbCurrencyComponent
  ],
})
export class GbCurrencyModule {}
