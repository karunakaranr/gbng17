import { Component, Input } from '@angular/core';

import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';

@Component({
  selector: 'app-gb-textarea',
  templateUrl: './gb-textarea.component.html',
  styleUrls: ['./gb-textarea.component.scss']
})
export class GbTextareaComponent  extends GBBaseUIFCComponent {
@Input() rows:string = '';;
}
