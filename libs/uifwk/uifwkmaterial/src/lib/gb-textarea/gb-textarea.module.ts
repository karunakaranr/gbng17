import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router, RouterModule } from '@angular/router';

import {GbTextareaComponent} from './gb-textarea.component';

@NgModule({
  declarations: [GbTextareaComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule
   ],
  exports: [GbTextareaComponent],

})
export class GbTextareaModule { }
