import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
// import { LayoutState } from 'features/layout/store/layout.state';
import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-radiobox',
  templateUrl: './gb-radiobox.component.html',
  styleUrls: ['./gb-radiobox.component.scss']
})
export class GbRadioboxComponent extends GBBaseUIFCComponent implements  OnChanges {
  // @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
  // @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Input() checkedid:any;
  @Input() picklistdata:any;
  @Output() childEvent: EventEmitter<any> = new EventEmitter();
  defaultvalue: any = {key:"",value:""};
  isreadable: any;
  onRadioButtonChange(event:any) { 
    this.childEvent.emit(event.value);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['checkedid'] || changes['picklistdata']){
      this.defaultvalue = this.picklistdata.find((c:any) => c.key == this.checkedid )
      if(this.checkedid == undefined){
        this.checkedid = 0;
        this.defaultvalue = this.picklistdata.find((c:any) => c.key == this.checkedid );
      }
    }
    // this.edit$.subscribe(res => {
    //   this.isreadable = res;
    // })
  }
}
