import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import {GbRadioboxComponent} from './gb-radiobox.component';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../../../../../features/transloco/transloco-root.module'
@NgModule({
  declarations: [GbRadioboxComponent],
  imports: [CommonModule, RouterModule, MatRadioModule, ReactiveFormsModule, TranslocoRootModule],
  exports: [GbRadioboxComponent],

})
export class GbRadioboxModule { }
