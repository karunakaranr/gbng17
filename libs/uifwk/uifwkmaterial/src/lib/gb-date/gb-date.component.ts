import { Component, Input } from '@angular/core';

import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';

@Component({
  selector: 'app-gb-date',
  templateUrl: './gb-date.component.html',
  styleUrls: ['./gb-date.component.scss']
})
export class GbDateComponent extends GBBaseUIFCComponent {
  // @Input() floatLabel; // 'always' | 'never' | 'auto' = 'always';
  // @Input() appearance; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
}

