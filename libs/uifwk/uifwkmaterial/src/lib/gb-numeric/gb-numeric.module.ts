import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { RouterModule } from '@angular/router';

import {GbInputModule} from './../gb-input/gb-input.module';
import {GbNumericComponent} from './gb-numeric.component';

@NgModule({
  declarations: [
    GbNumericComponent
  ],
  imports: [
    CommonModule,
    RouterModule ,
    MatFormFieldModule,
    ReactiveFormsModule,
    GbInputModule
  ],
  exports: [GbNumericComponent],

})
export class GbNumericModule { }
