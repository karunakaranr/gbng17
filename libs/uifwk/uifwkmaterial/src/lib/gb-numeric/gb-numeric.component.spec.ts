import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbNumericComponent } from './gb-numeric.component';

describe('GbNumericComponent', () => {
  let component: GbNumericComponent;
  let fixture: ComponentFixture<GbNumericComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbNumericComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbNumericComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
