import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-date',
  template: '{{Field?.Label}}: <input [formControl]="gbControl" type="date"><br/>'
})
export class GbDateComponent extends GBBaseUIFCComponent {
}
