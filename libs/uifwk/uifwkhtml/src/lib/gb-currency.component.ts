import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';


@Component({
  selector: 'app-gb-currency',
  template: '{{Field?.Label}}: <input [formControl]="gbControl" type="number"><br/>'
})
export class GbCurrencyComponent extends GBBaseUIFCComponent {
}
