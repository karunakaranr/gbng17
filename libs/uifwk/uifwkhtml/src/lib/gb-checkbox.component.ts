import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';


@Component({
  selector: 'app-gb-checkbox',
  template: '{{Field?.Label}}: <input [formControl]="gbControl" type="checkbox"><br/>'
})
export class GbCheckboxComponent extends GBBaseUIFCComponent {
}
