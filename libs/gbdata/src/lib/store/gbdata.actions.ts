import { ISelectListCriteria } from '../interfaces/ISectionCriteriaList';

export abstract class GBDataStateActionAbstract {
  // abstract readonly type: string;
}
export abstract class GBDataStateAction<T> extends GBDataStateActionAbstract {
  // abstract readonly type: string;
}

export abstract class GBDataStateActionFactory<T> {
  abstract GetData(id: number): GBDataStateAction<T>;
  abstract AddData(payload: T): GBDataStateAction<T>;
  abstract SaveData(payload: T): GBDataStateAction<T>;
  abstract ClearData(): GBDataStateAction<T>;
  abstract DeleteData(): GBDataStateAction<T>;
}
export abstract class GBDataStateActionFactoryWN<T> extends GBDataStateActionFactory<T> {
  abstract GetAll(scl: ISelectListCriteria): GBDataStateAction<T>;
  abstract MoveFirst(): GBDataStateAction<T>;
  abstract MovePrev(): GBDataStateAction<T>;
  abstract MoveNext(): GBDataStateAction<T>;
  abstract MoveLast(): GBDataStateAction<T>;
}

