import {
  NgComponentOutlet
} from "./chunk-GLDRZ4VJ.js";
import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver$1,
  Directive,
  ElementRef,
  EventEmitter,
  Host,
  Inject,
  Injectable,
  InjectionToken,
  Injector,
  Input,
  IterableDiffers,
  KeyValueDiffers,
  NgModule,
  Optional,
  Output,
  Renderer2,
  ViewContainerRef,
  setClassMetadata,
  ɵɵNgOnChangesFeature,
  ɵɵProvidersFeature,
  ɵɵStandaloneFeature,
  ɵɵdefineComponent,
  ɵɵdefineDirective,
  ɵɵdefineInjectable,
  ɵɵdefineInjector,
  ɵɵdefineNgModule,
  ɵɵdirectiveInject,
  ɵɵinject
} from "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import {
  Subject,
  takeUntil
} from "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";

// node_modules/ng-dynamic-component/fesm2020/ng-dynamic-component.mjs
function defaultEventArgumentFactory() {
  return "$event";
}
var IoEventArgumentToken = new InjectionToken("EventArgument", {
  providedIn: "root",
  factory: defaultEventArgumentFactory
});
var EventArgumentToken = IoEventArgumentToken;
var IoEventContextToken = new InjectionToken("IoEventContext");
var IoEventContextProviderToken = new InjectionToken("IoEventContextProvider");
var DynamicComponentInjectorToken = new InjectionToken("DynamicComponentInjector");
var IoServiceOptions = class {
  constructor() {
    this.trackOutputChanges = false;
  }
};
IoServiceOptions.ɵfac = function IoServiceOptions_Factory(t) {
  return new (t || IoServiceOptions)();
};
IoServiceOptions.ɵprov = ɵɵdefineInjectable({
  token: IoServiceOptions,
  factory: IoServiceOptions.ɵfac,
  providedIn: "root"
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(IoServiceOptions, [{
    type: Injectable,
    args: [{
      providedIn: "root"
    }]
  }], null, null);
})();
var IoService = class {
  constructor(injector, differs, cfr, options, compInjector, eventArgument, cdr, eventContextProvider) {
    this.injector = injector;
    this.differs = differs;
    this.cfr = cfr;
    this.options = options;
    this.compInjector = compInjector;
    this.eventArgument = eventArgument;
    this.cdr = cdr;
    this.eventContextProvider = eventContextProvider;
    this.lastComponentInst = null;
    this.lastChangedInputs = /* @__PURE__ */ new Set();
    this.inputsDiffer = this.differs.find({}).create();
    this.compFactory = null;
    this.outputsShouldDisconnect$ = new Subject();
    this.inputs = {};
    this.outputs = {};
    this.outputsChanged = () => false;
    if (this.options.trackOutputChanges) {
      const outputsDiffer = this.differs.find({}).create();
      this.outputsChanged = (outputs) => !!outputsDiffer.diff(outputs);
    }
  }
  get compRef() {
    return this.compInjector.componentRef;
  }
  get componentInst() {
    return this.compRef ? this.compRef.instance : null;
  }
  get componentInstChanged() {
    if (this.lastComponentInst !== this.componentInst) {
      this.lastComponentInst = this.componentInst;
      return true;
    } else {
      return false;
    }
  }
  ngOnDestroy() {
    this.disconnectOutputs();
  }
  /**
   * Call update whenever inputs/outputs may or did change.
   *
   * It will detect both new and mutated changes.
   */
  update(inputs, outputs) {
    if (!this.compRef) {
      this.disconnectOutputs();
      return;
    }
    const changes = this.updateIO(inputs, outputs);
    const compChanged = this.componentInstChanged;
    const inputsChanges = this.getInputsChanges(compChanged);
    const outputsChanged = this.outputsChanged(this.outputs);
    if (inputsChanges) {
      this.updateChangedInputs(inputsChanges);
    }
    if (compChanged || inputsChanges) {
      this.updateInputs(compChanged || !this.lastChangedInputs.size);
    }
    if (compChanged || outputsChanged || changes.outputsChanged) {
      this.bindOutputs();
    }
  }
  updateIO(inputs, outputs) {
    if (!inputs) {
      inputs = {};
    }
    if (!outputs) {
      outputs = {};
    }
    const inputsChanged = this.inputs !== inputs;
    const outputsChanged = this.outputs !== outputs;
    this.inputs = inputs;
    this.outputs = outputs;
    return {
      inputsChanged,
      outputsChanged
    };
  }
  updateInputs(isFirstChange = false) {
    if (isFirstChange) {
      this.updateCompFactory();
    }
    const compRef = this.compRef;
    const inputs = this.inputs;
    if (!inputs || !compRef) {
      return;
    }
    const ifInputChanged = this.lastChangedInputs.size ? (name) => this.lastChangedInputs.has(name) : () => true;
    Object.keys(inputs).filter(ifInputChanged).forEach((name) => compRef.setInput(name, inputs[name]));
  }
  bindOutputs() {
    this.disconnectOutputs();
    const compInst = this.componentInst;
    let outputs = this.outputs;
    if (!outputs || !compInst) {
      return;
    }
    outputs = this.resolveOutputs(outputs);
    Object.keys(outputs).filter((p) => compInst[p]).forEach((p) => compInst[p].pipe(takeUntil(this.outputsShouldDisconnect$)).subscribe((event) => {
      this.cdr.markForCheck();
      return outputs[p](event);
    }));
  }
  disconnectOutputs() {
    this.outputsShouldDisconnect$.next();
  }
  getInputsChanges(isCompChanged) {
    if (isCompChanged) {
      this.inputsDiffer.diff({});
    }
    return this.inputsDiffer.diff(this.inputs);
  }
  updateChangedInputs(differ) {
    this.lastChangedInputs.clear();
    const addRecordKeyToSet = (record) => this.lastChangedInputs.add(record.key);
    differ.forEachAddedItem(addRecordKeyToSet);
    differ.forEachChangedItem(addRecordKeyToSet);
    differ.forEachRemovedItem(addRecordKeyToSet);
  }
  // TODO: Replace ComponentFactory once new API is created
  // @see https://github.com/angular/angular/issues/44926
  // eslint-disable-next-line deprecation/deprecation
  resolveCompFactory() {
    if (!this.compRef) {
      return null;
    }
    try {
      try {
        return this.cfr.resolveComponentFactory(this.compRef.componentType);
      } catch (e) {
        return this.cfr.resolveComponentFactory(this.compRef.instance.constructor);
      }
    } catch (e) {
      return null;
    }
  }
  updateCompFactory() {
    this.compFactory = this.resolveCompFactory();
  }
  resolveOutputs(outputs) {
    this.updateOutputsEventContext();
    outputs = this.processOutputs(outputs);
    if (!this.compFactory) {
      return outputs;
    }
    return this.remapIO(outputs, this.compFactory.outputs);
  }
  updateOutputsEventContext() {
    if (this.eventContextProvider) {
      const eventContextInjector = Injector.create({
        name: "EventContext",
        parent: this.injector,
        providers: [this.eventContextProvider]
      });
      this.outputsEventContext = eventContextInjector.get(IoEventContextToken);
    } else {
      this.outputsEventContext = this.injector.get(IoEventContextToken, null);
    }
  }
  processOutputs(outputs) {
    const processedOutputs = {};
    Object.keys(outputs).forEach((key) => {
      const outputExpr = outputs[key];
      let outputHandler;
      if (typeof outputExpr === "function") {
        outputHandler = outputExpr;
      } else {
        outputHandler = outputExpr && this.processOutputArgs(outputExpr);
      }
      if (this.outputsEventContext && outputHandler) {
        outputHandler = outputHandler.bind(this.outputsEventContext);
      }
      processedOutputs[key] = outputHandler;
    });
    return processedOutputs;
  }
  processOutputArgs(output) {
    const eventArgument = this.eventArgument;
    const args = "args" in output ? output.args || [] : [eventArgument];
    const eventIdx = args.indexOf(eventArgument);
    const handler = output.handler;
    if (eventIdx === -1) {
      return function() {
        return handler.apply(this, args);
      };
    }
    return function(event) {
      const argsWithEvent = [...args];
      argsWithEvent[eventIdx] = event;
      return handler.apply(this, argsWithEvent);
    };
  }
  remapIO(io, mapping) {
    const newIO = {};
    Object.keys(io).forEach((key) => {
      const newKey = this.findPropByTplInMapping(key, mapping) || key;
      newIO[newKey] = io[key];
    });
    return newIO;
  }
  findPropByTplInMapping(tplName, mapping) {
    for (const map of mapping) {
      if (map.templateName === tplName) {
        return map.propName;
      }
    }
    return null;
  }
};
IoService.ɵfac = function IoService_Factory(t) {
  return new (t || IoService)(ɵɵinject(Injector), ɵɵinject(KeyValueDiffers), ɵɵinject(ComponentFactoryResolver$1), ɵɵinject(IoServiceOptions), ɵɵinject(DynamicComponentInjectorToken), ɵɵinject(IoEventArgumentToken), ɵɵinject(ChangeDetectorRef), ɵɵinject(IoEventContextProviderToken, 8));
};
IoService.ɵprov = ɵɵdefineInjectable({
  token: IoService,
  factory: IoService.ɵfac
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(IoService, [{
    type: Injectable
  }], function() {
    return [{
      type: Injector
    }, {
      type: KeyValueDiffers
    }, {
      type: ComponentFactoryResolver$1
    }, {
      type: IoServiceOptions
    }, {
      type: void 0,
      decorators: [{
        type: Inject,
        args: [DynamicComponentInjectorToken]
      }]
    }, {
      type: void 0,
      decorators: [{
        type: Inject,
        args: [IoEventArgumentToken]
      }]
    }, {
      type: ChangeDetectorRef
    }, {
      type: void 0,
      decorators: [{
        type: Inject,
        args: [IoEventContextProviderToken]
      }, {
        type: Optional
      }]
    }];
  }, null);
})();
var IoFactoryService = class {
  constructor(injector) {
    this.injector = injector;
  }
  create(componentInjector, ioOptions) {
    const providers = [{
      provide: IoService,
      useClass: IoService
    }, {
      provide: DynamicComponentInjectorToken,
      useValue: componentInjector
    }];
    if (ioOptions) {
      providers.push({
        provide: IoServiceOptions,
        useValue: ioOptions
      });
    }
    const ioInjector = Injector.create({
      name: "IoInjector",
      parent: ioOptions?.injector ?? this.injector,
      providers
    });
    return ioInjector.get(IoService);
  }
};
IoFactoryService.ɵfac = function IoFactoryService_Factory(t) {
  return new (t || IoFactoryService)(ɵɵinject(Injector));
};
IoFactoryService.ɵprov = ɵɵdefineInjectable({
  token: IoFactoryService,
  factory: IoFactoryService.ɵfac,
  providedIn: "root"
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(IoFactoryService, [{
    type: Injectable,
    args: [{
      providedIn: "root"
    }]
  }], function() {
    return [{
      type: Injector
    }];
  }, null);
})();
var ComponentOutletIoDirective = class {
  constructor(ioService) {
    this.ioService = ioService;
  }
  ngDoCheck() {
    this.ioService.update(this.ngComponentOutletNdcDynamicInputs, this.ngComponentOutletNdcDynamicOutputs);
  }
};
ComponentOutletIoDirective.ɵfac = function ComponentOutletIoDirective_Factory(t) {
  return new (t || ComponentOutletIoDirective)(ɵɵdirectiveInject(IoService));
};
ComponentOutletIoDirective.ɵdir = ɵɵdefineDirective({
  type: ComponentOutletIoDirective,
  selectors: [["", "ngComponentOutletNdcDynamicInputs", ""], ["", "ngComponentOutletNdcDynamicOutputs", ""]],
  inputs: {
    ngComponentOutletNdcDynamicInputs: "ngComponentOutletNdcDynamicInputs",
    ngComponentOutletNdcDynamicOutputs: "ngComponentOutletNdcDynamicOutputs"
  },
  exportAs: ["ndcDynamicIo"],
  standalone: true,
  features: [ɵɵProvidersFeature([IoService])]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(ComponentOutletIoDirective, [{
    type: Directive,
    args: [{
      selector: (
        // eslint-disable-next-line @angular-eslint/directive-selector
        "[ngComponentOutletNdcDynamicInputs],[ngComponentOutletNdcDynamicOutputs]"
      ),
      exportAs: "ndcDynamicIo",
      standalone: true,
      providers: [IoService]
    }]
  }], function() {
    return [{
      type: IoService
    }];
  }, {
    ngComponentOutletNdcDynamicInputs: [{
      type: Input
    }],
    ngComponentOutletNdcDynamicOutputs: [{
      type: Input
    }]
  });
})();
var ComponentOutletInjectorDirective = class {
  constructor(componentOutlet) {
    this.componentOutlet = componentOutlet;
  }
  get componentRef() {
    return this.componentOutlet._componentRef;
  }
};
ComponentOutletInjectorDirective.ɵfac = function ComponentOutletInjectorDirective_Factory(t) {
  return new (t || ComponentOutletInjectorDirective)(ɵɵdirectiveInject(NgComponentOutlet, 1));
};
ComponentOutletInjectorDirective.ɵdir = ɵɵdefineDirective({
  type: ComponentOutletInjectorDirective,
  selectors: [["", "ngComponentOutlet", ""]],
  exportAs: ["ndcComponentOutletInjector"],
  standalone: true,
  features: [ɵɵProvidersFeature([{
    provide: DynamicComponentInjectorToken,
    useExisting: ComponentOutletInjectorDirective
  }])]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(ComponentOutletInjectorDirective, [{
    type: Directive,
    args: [{
      // eslint-disable-next-line @angular-eslint/directive-selector
      selector: "[ngComponentOutlet]",
      exportAs: "ndcComponentOutletInjector",
      standalone: true,
      providers: [{
        provide: DynamicComponentInjectorToken,
        useExisting: ComponentOutletInjectorDirective
      }]
    }]
  }], function() {
    return [{
      type: NgComponentOutlet,
      decorators: [{
        type: Host
      }]
    }];
  }, null);
})();
var ComponentOutletInjectorModule = class {
};
ComponentOutletInjectorModule.ɵfac = function ComponentOutletInjectorModule_Factory(t) {
  return new (t || ComponentOutletInjectorModule)();
};
ComponentOutletInjectorModule.ɵmod = ɵɵdefineNgModule({
  type: ComponentOutletInjectorModule,
  imports: [ComponentOutletInjectorDirective, ComponentOutletIoDirective],
  exports: [ComponentOutletInjectorDirective, ComponentOutletIoDirective]
});
ComponentOutletInjectorModule.ɵinj = ɵɵdefineInjector({});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(ComponentOutletInjectorModule, [{
    type: NgModule,
    args: [{
      imports: [ComponentOutletInjectorDirective, ComponentOutletIoDirective],
      exports: [ComponentOutletInjectorDirective, ComponentOutletIoDirective]
    }]
  }], null, null);
})();
var DynamicIoDirective = class {
  constructor(ioService) {
    this.ioService = ioService;
  }
  ngDoCheck() {
    this.ioService.update(this.ndcDynamicInputs, this.ndcDynamicOutputs);
  }
};
DynamicIoDirective.ɵfac = function DynamicIoDirective_Factory(t) {
  return new (t || DynamicIoDirective)(ɵɵdirectiveInject(IoService));
};
DynamicIoDirective.ɵdir = ɵɵdefineDirective({
  type: DynamicIoDirective,
  selectors: [["", "ndcDynamicInputs", ""], ["", "ndcDynamicOutputs", ""]],
  inputs: {
    ndcDynamicInputs: "ndcDynamicInputs",
    ndcDynamicOutputs: "ndcDynamicOutputs"
  },
  exportAs: ["ndcDynamicIo"],
  standalone: true,
  features: [ɵɵProvidersFeature([IoService])]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicIoDirective, [{
    type: Directive,
    args: [{
      selector: "[ndcDynamicInputs],[ndcDynamicOutputs]",
      exportAs: "ndcDynamicIo",
      standalone: true,
      providers: [IoService]
    }]
  }], function() {
    return [{
      type: IoService
    }];
  }, {
    ndcDynamicInputs: [{
      type: Input
    }],
    ndcDynamicOutputs: [{
      type: Input
    }]
  });
})();
var DynamicIoModule = class {
};
DynamicIoModule.ɵfac = function DynamicIoModule_Factory(t) {
  return new (t || DynamicIoModule)();
};
DynamicIoModule.ɵmod = ɵɵdefineNgModule({
  type: DynamicIoModule,
  imports: [DynamicIoDirective],
  exports: [DynamicIoDirective, ComponentOutletInjectorModule]
});
DynamicIoModule.ɵinj = ɵɵdefineInjector({
  imports: [ComponentOutletInjectorModule]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicIoModule, [{
    type: NgModule,
    args: [{
      imports: [DynamicIoDirective],
      exports: [DynamicIoDirective, ComponentOutletInjectorModule]
    }]
  }], null, null);
})();
var DynamicComponent = class _DynamicComponent {
  constructor(vcr) {
    this.vcr = vcr;
    this.ndcDynamicCreated = new EventEmitter();
    this.componentRef = null;
  }
  ngOnChanges(changes) {
    if (_DynamicComponent.UpdateOnInputs.some((input) => changes.hasOwnProperty(input))) {
      this.createDynamicComponent();
    }
  }
  createDynamicComponent() {
    this.vcr.clear();
    this.componentRef = null;
    if (this.ndcDynamicComponent) {
      this.componentRef = this.vcr.createComponent(this.ndcDynamicComponent, {
        index: 0,
        injector: this._resolveInjector(),
        projectableNodes: this.ndcDynamicContent,
        ngModuleRef: this.ndcDynamicNgModuleRef,
        environmentInjector: this.ndcDynamicEnvironmentInjector
      });
      this.ndcDynamicCreated.emit(this.componentRef);
    }
  }
  _resolveInjector() {
    let injector = this.ndcDynamicInjector || this.vcr.injector;
    if (this.ndcDynamicProviders) {
      injector = Injector.create({
        providers: this.ndcDynamicProviders,
        parent: injector
      });
    }
    return injector;
  }
};
DynamicComponent.UpdateOnInputs = ["ndcDynamicComponent", "ndcDynamicInjector", "ndcDynamicProviders", "ndcDynamicContent", "ndcDynamicNgModuleRef", "ndcDynamicEnvironmentInjector"];
DynamicComponent.ɵfac = function DynamicComponent_Factory(t) {
  return new (t || DynamicComponent)(ɵɵdirectiveInject(ViewContainerRef));
};
DynamicComponent.ɵcmp = ɵɵdefineComponent({
  type: DynamicComponent,
  selectors: [["ndc-dynamic"]],
  inputs: {
    ndcDynamicComponent: "ndcDynamicComponent",
    ndcDynamicInjector: "ndcDynamicInjector",
    ndcDynamicProviders: "ndcDynamicProviders",
    ndcDynamicContent: "ndcDynamicContent",
    ndcDynamicNgModuleRef: "ndcDynamicNgModuleRef",
    ndcDynamicEnvironmentInjector: "ndcDynamicEnvironmentInjector"
  },
  outputs: {
    ndcDynamicCreated: "ndcDynamicCreated"
  },
  standalone: true,
  features: [ɵɵProvidersFeature([{
    provide: DynamicComponentInjectorToken,
    useExisting: DynamicComponent
  }]), ɵɵNgOnChangesFeature, ɵɵStandaloneFeature],
  decls: 0,
  vars: 0,
  template: function DynamicComponent_Template(rf, ctx) {
  },
  encapsulation: 2
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicComponent, [{
    type: Component,
    args: [{
      selector: "ndc-dynamic",
      standalone: true,
      template: "",
      providers: [{
        provide: DynamicComponentInjectorToken,
        useExisting: DynamicComponent
      }]
    }]
  }], function() {
    return [{
      type: ViewContainerRef
    }];
  }, {
    ndcDynamicComponent: [{
      type: Input
    }],
    ndcDynamicInjector: [{
      type: Input
    }],
    ndcDynamicProviders: [{
      type: Input
    }],
    ndcDynamicContent: [{
      type: Input
    }],
    ndcDynamicNgModuleRef: [{
      type: Input
    }],
    ndcDynamicEnvironmentInjector: [{
      type: Input
    }],
    ndcDynamicCreated: [{
      type: Output
    }]
  });
})();
var DynamicModule = class {
};
DynamicModule.ɵfac = function DynamicModule_Factory(t) {
  return new (t || DynamicModule)();
};
DynamicModule.ɵmod = ɵɵdefineNgModule({
  type: DynamicModule,
  imports: [DynamicIoModule, DynamicComponent],
  exports: [DynamicIoModule, DynamicComponent]
});
DynamicModule.ɵinj = ɵɵdefineInjector({
  imports: [DynamicIoModule, DynamicComponent, DynamicIoModule]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicModule, [{
    type: NgModule,
    args: [{
      imports: [DynamicIoModule, DynamicComponent],
      exports: [DynamicIoModule, DynamicComponent]
    }]
  }], null, null);
})();
var DynamicAttributesDirective = class {
  constructor(renderer, differs, componentInjector) {
    this.renderer = renderer;
    this.differs = differs;
    this.componentInjector = componentInjector;
    this.attrsDiffer = this.differs.find({}).create();
  }
  get _attributes() {
    return this.ndcDynamicAttributes || this.ngComponentOutletNdcDynamicAttributes || {};
  }
  get nativeElement() {
    return this.componentInjector?.componentRef?.location.nativeElement;
  }
  get compType() {
    return this.componentInjector?.componentRef?.componentType;
  }
  get isCompChanged() {
    if (this.lastCompType !== this.compType) {
      this.lastCompType = this.compType;
      return true;
    }
    return false;
  }
  ngDoCheck() {
    const isCompChanged = this.isCompChanged;
    const changes = this.attrsDiffer.diff(this._attributes);
    if (changes) {
      this.lastAttrActions = this.changesToAttrActions(changes);
    }
    if (changes || isCompChanged && this.lastAttrActions) {
      this.updateAttributes(this.lastAttrActions);
    }
  }
  setAttribute(name, value, namespace) {
    if (this.nativeElement) {
      this.renderer.setAttribute(this.nativeElement, name, value, namespace);
    }
  }
  removeAttribute(name, namespace) {
    if (this.nativeElement) {
      this.renderer.removeAttribute(this.nativeElement, name, namespace);
    }
  }
  updateAttributes(actions) {
    if (!this.compType || !actions) {
      return;
    }
    Object.keys(actions.set).forEach((key) => this.setAttribute(key, actions.set[key]));
    actions.remove.forEach((key) => this.removeAttribute(key));
  }
  changesToAttrActions(changes) {
    const attrActions = {
      set: {},
      remove: []
    };
    changes.forEachAddedItem((r) => attrActions.set[r.key] = r.currentValue);
    changes.forEachChangedItem((r) => attrActions.set[r.key] = r.currentValue);
    changes.forEachRemovedItem((r) => attrActions.remove.push(r.key));
    return attrActions;
  }
};
DynamicAttributesDirective.ɵfac = function DynamicAttributesDirective_Factory(t) {
  return new (t || DynamicAttributesDirective)(ɵɵdirectiveInject(Renderer2), ɵɵdirectiveInject(KeyValueDiffers), ɵɵdirectiveInject(DynamicComponentInjectorToken, 8));
};
DynamicAttributesDirective.ɵdir = ɵɵdefineDirective({
  type: DynamicAttributesDirective,
  selectors: [["", "ndcDynamicAttributes", ""], ["", "ngComponentOutletNdcDynamicAttributes", ""]],
  inputs: {
    ndcDynamicAttributes: "ndcDynamicAttributes",
    ngComponentOutletNdcDynamicAttributes: "ngComponentOutletNdcDynamicAttributes"
  },
  exportAs: ["ndcDynamicAttributes"],
  standalone: true
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicAttributesDirective, [{
    type: Directive,
    args: [{
      selector: "[ndcDynamicAttributes],[ngComponentOutletNdcDynamicAttributes]",
      exportAs: "ndcDynamicAttributes",
      standalone: true
    }]
  }], function() {
    return [{
      type: Renderer2
    }, {
      type: KeyValueDiffers
    }, {
      type: void 0,
      decorators: [{
        type: Inject,
        args: [DynamicComponentInjectorToken]
      }, {
        type: Optional
      }]
    }];
  }, {
    ndcDynamicAttributes: [{
      type: Input
    }],
    ngComponentOutletNdcDynamicAttributes: [{
      type: Input
    }]
  });
})();
var DynamicAttributesModule = class {
};
DynamicAttributesModule.ɵfac = function DynamicAttributesModule_Factory(t) {
  return new (t || DynamicAttributesModule)();
};
DynamicAttributesModule.ɵmod = ɵɵdefineNgModule({
  type: DynamicAttributesModule,
  imports: [DynamicAttributesDirective],
  exports: [DynamicAttributesDirective, ComponentOutletInjectorModule]
});
DynamicAttributesModule.ɵinj = ɵɵdefineInjector({
  imports: [ComponentOutletInjectorModule]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicAttributesModule, [{
    type: NgModule,
    args: [{
      imports: [DynamicAttributesDirective],
      exports: [DynamicAttributesDirective, ComponentOutletInjectorModule]
    }]
  }], null, null);
})();
function extractNgParamTypes(type) {
  return type?.ctorParameters?.()?.map((param) => param.type);
}
function isOnDestroy(obj) {
  return !!obj && typeof obj.ngOnDestroy === "function";
}
var ReflectRef = new InjectionToken("ReflectRef", {
  providedIn: "root",
  factory: () => window.Reflect
});
var ReflectService = class {
  constructor(reflect) {
    this.reflect = reflect;
  }
  getCtorParamTypes(ctor) {
    return this.reflect.getMetadata("design:paramtypes", ctor);
  }
};
ReflectService.ɵfac = function ReflectService_Factory(t) {
  return new (t || ReflectService)(ɵɵinject(ReflectRef));
};
ReflectService.ɵprov = ɵɵdefineInjectable({
  token: ReflectService,
  factory: ReflectService.ɵfac,
  providedIn: "root"
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(ReflectService, [{
    type: Injectable,
    args: [{
      providedIn: "root"
    }]
  }], function() {
    return [{
      type: void 0,
      decorators: [{
        type: Inject,
        args: [ReflectRef]
      }]
    }];
  }, null);
})();
function dynamicDirectiveDef(type, inputs, outputs) {
  return {
    type,
    inputs,
    outputs
  };
}
var DynamicDirectivesDirective = class {
  constructor(injector, iterableDiffers, ioFactoryService, reflectService, componentInjector) {
    this.injector = injector;
    this.iterableDiffers = iterableDiffers;
    this.ioFactoryService = ioFactoryService;
    this.reflectService = reflectService;
    this.componentInjector = componentInjector;
    this.ndcDynamicDirectivesCreated = new EventEmitter();
    this.dirRef = /* @__PURE__ */ new Map();
    this.dirIo = /* @__PURE__ */ new Map();
    this.dirsDiffer = this.iterableDiffers.find([]).create((_, def) => def.type);
  }
  get directives() {
    return this.ndcDynamicDirectives || this.ngComponentOutletNdcDynamicDirectives;
  }
  get componentRef() {
    return this.componentInjector?.componentRef;
  }
  get compInstance() {
    return this.componentRef && this.componentRef.instance;
  }
  get isCompChanged() {
    if (this.lastCompInstance !== this.compInstance) {
      this.lastCompInstance = this.compInstance;
      return true;
    }
    return false;
  }
  get hostInjector() {
    return this.componentRef?.injector;
  }
  ngDoCheck() {
    if (this.maybeDestroyDirectives()) {
      return;
    }
    const dirsChanges = this.dirsDiffer.diff(this.directives);
    if (!dirsChanges) {
      return this.updateDirectives();
    }
    this.processDirChanges(dirsChanges);
  }
  ngOnDestroy() {
    this.destroyAllDirectives();
  }
  maybeDestroyDirectives() {
    if (this.isCompChanged || !this.componentRef) {
      this.dirsDiffer.diff([]);
      this.destroyAllDirectives();
    }
    return !this.componentRef;
  }
  processDirChanges(changes) {
    changes.forEachRemovedItem(({
      item
    }) => this.destroyDirective(item));
    const createdDirs = [];
    changes.forEachAddedItem(({
      item
    }) => {
      const dirRef = this.initDirective(item);
      if (dirRef) {
        createdDirs.push(dirRef);
      }
    });
    if (createdDirs.length) {
      this.ndcDynamicDirectivesCreated.emit(createdDirs);
    }
  }
  updateDirectives() {
    this.directives?.forEach((dir) => this.updateDirective(dir));
  }
  updateDirective(dirDef) {
    const io = this.dirIo.get(dirDef.type);
    io?.update(dirDef.inputs, dirDef.outputs);
  }
  initDirective(dirDef) {
    if (this.dirRef.has(dirDef.type)) {
      return;
    }
    const instance = this.createDirective(dirDef.type);
    const directiveRef = {
      instance,
      type: dirDef.type,
      injector: this.hostInjector,
      hostComponent: this.componentRef.instance,
      hostView: this.componentRef.hostView,
      location: this.componentRef.location,
      changeDetectorRef: this.componentRef.changeDetectorRef,
      onDestroy: this.componentRef.onDestroy
    };
    this.initDirIO(directiveRef, dirDef);
    this.callInitHooks(instance);
    this.dirRef.set(directiveRef.type, directiveRef);
    return directiveRef;
  }
  destroyAllDirectives() {
    this.dirRef.forEach((dir) => this.destroyDirRef(dir));
    this.dirRef.clear();
    this.dirIo.clear();
  }
  destroyDirective(dirDef) {
    const dirRef = this.dirRef.get(dirDef.type);
    if (dirRef) {
      this.destroyDirRef(dirRef);
    }
    this.dirRef.delete(dirDef.type);
    this.dirIo.delete(dirDef.type);
  }
  initDirIO(dirRef, dirDef) {
    const io = this.ioFactoryService.create({
      componentRef: this.dirToCompDef(dirRef)
    }, {
      trackOutputChanges: true,
      injector: this.injector
    });
    io.update(dirDef.inputs, dirDef.outputs);
    this.dirIo.set(dirRef.type, io);
  }
  dirToCompDef(dirRef) {
    return {
      changeDetectorRef: this.componentRef.changeDetectorRef,
      hostView: this.componentRef.hostView,
      location: this.componentRef.location,
      destroy: this.componentRef.destroy,
      onDestroy: this.componentRef.onDestroy,
      injector: this.componentRef.injector,
      instance: dirRef.instance,
      componentType: dirRef.type,
      setInput: (name, value) => dirRef.instance[name] = value
    };
  }
  destroyDirRef(dirRef) {
    this.dirIo.get(dirRef.type)?.ngOnDestroy();
    if (isOnDestroy(dirRef.instance)) {
      dirRef.instance.ngOnDestroy();
    }
  }
  createDirective(dirType) {
    const directiveInjector = Injector.create({
      providers: [{
        provide: dirType,
        useClass: dirType,
        deps: this.resolveDirParamTypes(dirType)
      }, {
        provide: ElementRef,
        useValue: this.componentRef.location
      }],
      parent: this.hostInjector,
      name: `DynamicDirectiveInjector:${dirType.name}@${this.componentRef.componentType.name}`
    });
    return directiveInjector.get(dirType);
  }
  resolveDirParamTypes(dirType) {
    return (
      // First try Angular Compiler's metadata
      extractNgParamTypes(dirType) ?? // Then fallback to Reflect API
      this.reflectService.getCtorParamTypes(dirType) ?? // Bailout
      []
    );
  }
  callInitHooks(obj) {
    this.callHook(obj, "ngOnInit");
    this.callHook(obj, "ngDoCheck");
    this.callHook(obj, "ngAfterContentInit");
    this.callHook(obj, "ngAfterContentChecked");
    this.callHook(obj, "ngAfterViewInit");
    this.callHook(obj, "ngAfterViewChecked");
  }
  callHook(obj, hook, args = []) {
    if (obj[hook]) {
      obj[hook](...args);
    }
  }
};
DynamicDirectivesDirective.ɵfac = function DynamicDirectivesDirective_Factory(t) {
  return new (t || DynamicDirectivesDirective)(ɵɵdirectiveInject(Injector), ɵɵdirectiveInject(IterableDiffers), ɵɵdirectiveInject(IoFactoryService), ɵɵdirectiveInject(ReflectService), ɵɵdirectiveInject(DynamicComponentInjectorToken, 8));
};
DynamicDirectivesDirective.ɵdir = ɵɵdefineDirective({
  type: DynamicDirectivesDirective,
  selectors: [["", "ndcDynamicDirectives", ""], ["", "ngComponentOutletNdcDynamicDirectives", ""]],
  inputs: {
    ndcDynamicDirectives: "ndcDynamicDirectives",
    ngComponentOutletNdcDynamicDirectives: "ngComponentOutletNdcDynamicDirectives"
  },
  outputs: {
    ndcDynamicDirectivesCreated: "ndcDynamicDirectivesCreated"
  },
  standalone: true,
  features: [ɵɵProvidersFeature([IoFactoryService])]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicDirectivesDirective, [{
    type: Directive,
    args: [{
      selector: "[ndcDynamicDirectives],[ngComponentOutletNdcDynamicDirectives]",
      standalone: true,
      providers: [IoFactoryService]
    }]
  }], function() {
    return [{
      type: Injector
    }, {
      type: IterableDiffers
    }, {
      type: IoFactoryService
    }, {
      type: ReflectService
    }, {
      type: void 0,
      decorators: [{
        type: Inject,
        args: [DynamicComponentInjectorToken]
      }, {
        type: Optional
      }]
    }];
  }, {
    ndcDynamicDirectives: [{
      type: Input
    }],
    ngComponentOutletNdcDynamicDirectives: [{
      type: Input
    }],
    ndcDynamicDirectivesCreated: [{
      type: Output
    }]
  });
})();
var DynamicDirectivesModule = class {
};
DynamicDirectivesModule.ɵfac = function DynamicDirectivesModule_Factory(t) {
  return new (t || DynamicDirectivesModule)();
};
DynamicDirectivesModule.ɵmod = ɵɵdefineNgModule({
  type: DynamicDirectivesModule,
  imports: [DynamicDirectivesDirective],
  exports: [DynamicDirectivesDirective, ComponentOutletInjectorModule]
});
DynamicDirectivesModule.ɵinj = ɵɵdefineInjector({
  imports: [ComponentOutletInjectorModule]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(DynamicDirectivesModule, [{
    type: NgModule,
    args: [{
      imports: [DynamicDirectivesDirective],
      exports: [DynamicDirectivesDirective, ComponentOutletInjectorModule]
    }]
  }], null, null);
})();
export {
  ComponentOutletInjectorDirective,
  ComponentOutletInjectorModule,
  ComponentOutletIoDirective,
  DynamicAttributesDirective,
  DynamicAttributesModule,
  DynamicComponent,
  DynamicComponentInjectorToken,
  DynamicDirectivesDirective,
  DynamicDirectivesModule,
  DynamicIoDirective,
  DynamicIoModule,
  DynamicModule,
  EventArgumentToken,
  IoEventArgumentToken,
  IoEventContextProviderToken,
  IoEventContextToken,
  IoFactoryService,
  IoService,
  IoServiceOptions,
  ReflectRef,
  ReflectService,
  defaultEventArgumentFactory,
  dynamicDirectiveDef
};
//# sourceMappingURL=ng-dynamic-component.js.map
