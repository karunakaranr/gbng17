import {
  BrowserAnimationsModule,
  InjectableAnimationEngine,
  NoopAnimationsModule,
  provideAnimations,
  provideNoopAnimations
} from "./chunk-2M4B6PU2.js";
import "./chunk-7PLXW5PR.js";
import "./chunk-55OKLQ4U.js";
import "./chunk-4L62CIWV.js";
import "./chunk-GLDRZ4VJ.js";
import {
  ANIMATION_MODULE_TYPE
} from "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  ANIMATION_MODULE_TYPE,
  BrowserAnimationsModule,
  NoopAnimationsModule,
  provideAnimations,
  provideNoopAnimations,
  InjectableAnimationEngine as ɵInjectableAnimationEngine
};
//# sourceMappingURL=@angular_platform-browser_animations.js.map
