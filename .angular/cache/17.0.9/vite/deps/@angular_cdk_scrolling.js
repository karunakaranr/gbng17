import {
  CdkFixedSizeVirtualScroll,
  CdkScrollable,
  CdkScrollableModule,
  CdkVirtualForOf,
  CdkVirtualScrollViewport,
  CdkVirtualScrollable,
  CdkVirtualScrollableElement,
  CdkVirtualScrollableWindow,
  DEFAULT_RESIZE_TIME,
  DEFAULT_SCROLL_TIME,
  FixedSizeVirtualScrollStrategy,
  ScrollDispatcher,
  ScrollingModule,
  VIRTUAL_SCROLLABLE,
  VIRTUAL_SCROLL_STRATEGY,
  ViewportRuler,
  _fixedSizeVirtualScrollStrategyFactory
} from "./chunk-TUWB2S4O.js";
import "./chunk-NN72S56S.js";
import "./chunk-GQE2HGQ7.js";
import "./chunk-4WQH3DGR.js";
import "./chunk-MCBQ7T4L.js";
import "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  CdkFixedSizeVirtualScroll,
  CdkScrollable,
  CdkScrollableModule,
  CdkVirtualForOf,
  CdkVirtualScrollViewport,
  CdkVirtualScrollable,
  CdkVirtualScrollableElement,
  CdkVirtualScrollableWindow,
  DEFAULT_RESIZE_TIME,
  DEFAULT_SCROLL_TIME,
  FixedSizeVirtualScrollStrategy,
  ScrollDispatcher,
  ScrollingModule,
  VIRTUAL_SCROLLABLE,
  VIRTUAL_SCROLL_STRATEGY,
  ViewportRuler,
  _fixedSizeVirtualScrollStrategyFactory
};
//# sourceMappingURL=@angular_cdk_scrolling.js.map
