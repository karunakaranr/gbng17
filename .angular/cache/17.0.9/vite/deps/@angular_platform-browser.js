import {
  BrowserDomAdapter,
  BrowserGetTestability,
  BrowserModule,
  By,
  DomEventsPlugin,
  DomRendererFactory2,
  DomSanitizer,
  DomSanitizerImpl,
  EVENT_MANAGER_PLUGINS,
  EventManager,
  EventManagerPlugin,
  HAMMER_GESTURE_CONFIG,
  HAMMER_LOADER,
  HammerGestureConfig,
  HammerGesturesPlugin,
  HammerModule,
  INTERNAL_BROWSER_PLATFORM_PROVIDERS,
  KeyEventsPlugin,
  Meta,
  REMOVE_STYLES_ON_COMPONENT_DESTROY,
  SharedStylesHost,
  Title,
  TransferState,
  VERSION,
  bootstrapApplication,
  createApplication,
  disableDebugTools,
  enableDebugTools,
  initDomAdapter,
  makeStateKey,
  platformBrowser,
  provideClientHydration,
  provideProtractorTestingSupport,
  withHttpTransferCacheOptions,
  withNoHttpTransferCache
} from "./chunk-55OKLQ4U.js";
import "./chunk-4L62CIWV.js";
import {
  getDOM
} from "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  BrowserModule,
  By,
  DomSanitizer,
  EVENT_MANAGER_PLUGINS,
  EventManager,
  EventManagerPlugin,
  HAMMER_GESTURE_CONFIG,
  HAMMER_LOADER,
  HammerGestureConfig,
  HammerModule,
  Meta,
  REMOVE_STYLES_ON_COMPONENT_DESTROY,
  Title,
  TransferState,
  VERSION,
  bootstrapApplication,
  createApplication,
  disableDebugTools,
  enableDebugTools,
  makeStateKey,
  platformBrowser,
  provideClientHydration,
  provideProtractorTestingSupport,
  withHttpTransferCacheOptions,
  withNoHttpTransferCache,
  BrowserDomAdapter as ɵBrowserDomAdapter,
  BrowserGetTestability as ɵBrowserGetTestability,
  DomEventsPlugin as ɵDomEventsPlugin,
  DomRendererFactory2 as ɵDomRendererFactory2,
  DomSanitizerImpl as ɵDomSanitizerImpl,
  HammerGesturesPlugin as ɵHammerGesturesPlugin,
  INTERNAL_BROWSER_PLATFORM_PROVIDERS as ɵINTERNAL_BROWSER_PLATFORM_PROVIDERS,
  KeyEventsPlugin as ɵKeyEventsPlugin,
  SharedStylesHost as ɵSharedStylesHost,
  getDOM as ɵgetDOM,
  initDomAdapter as ɵinitDomAdapter
};
//# sourceMappingURL=@angular_platform-browser.js.map
