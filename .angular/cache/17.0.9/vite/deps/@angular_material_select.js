import {
  MAT_SELECT_CONFIG,
  MAT_SELECT_SCROLL_STRATEGY,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY,
  MAT_SELECT_TRIGGER,
  MatSelect,
  MatSelectChange,
  MatSelectModule,
  MatSelectTrigger,
  matSelectAnimations
} from "./chunk-NZECVFRZ.js";
import "./chunk-RL4HLGNP.js";
import "./chunk-IYDN2AE5.js";
import "./chunk-JLD3QCX6.js";
import "./chunk-2M4B6PU2.js";
import "./chunk-7PLXW5PR.js";
import "./chunk-55OKLQ4U.js";
import "./chunk-4L62CIWV.js";
import "./chunk-ITMNTLG4.js";
import "./chunk-HGBWRQPT.js";
import "./chunk-TUWB2S4O.js";
import "./chunk-GARXXTCJ.js";
import "./chunk-QEKODB4M.js";
import "./chunk-NN72S56S.js";
import "./chunk-GQE2HGQ7.js";
import "./chunk-4WQH3DGR.js";
import "./chunk-MCBQ7T4L.js";
import "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  MAT_SELECT_CONFIG,
  MAT_SELECT_SCROLL_STRATEGY,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY,
  MAT_SELECT_TRIGGER,
  MatSelect,
  MatSelectChange,
  MatSelectModule,
  MatSelectTrigger,
  matSelectAnimations
};
//# sourceMappingURL=@angular_material_select.js.map
