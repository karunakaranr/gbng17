import {
  ICON_REGISTRY_PROVIDER,
  ICON_REGISTRY_PROVIDER_FACTORY,
  MAT_ICON_DEFAULT_OPTIONS,
  MAT_ICON_LOCATION,
  MAT_ICON_LOCATION_FACTORY,
  MatIcon,
  MatIconModule,
  MatIconRegistry,
  getMatIconFailedToSanitizeLiteralError,
  getMatIconFailedToSanitizeUrlError,
  getMatIconNameNotFoundError,
  getMatIconNoHttpProviderError
} from "./chunk-2TCMCUC2.js";
import "./chunk-JLD3QCX6.js";
import "./chunk-2M4B6PU2.js";
import "./chunk-7PLXW5PR.js";
import "./chunk-55OKLQ4U.js";
import "./chunk-4L62CIWV.js";
import "./chunk-GARXXTCJ.js";
import "./chunk-QEKODB4M.js";
import "./chunk-NN72S56S.js";
import "./chunk-4WQH3DGR.js";
import "./chunk-MCBQ7T4L.js";
import "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  ICON_REGISTRY_PROVIDER,
  ICON_REGISTRY_PROVIDER_FACTORY,
  MAT_ICON_DEFAULT_OPTIONS,
  MAT_ICON_LOCATION,
  MAT_ICON_LOCATION_FACTORY,
  MatIcon,
  MatIconModule,
  MatIconRegistry,
  getMatIconFailedToSanitizeLiteralError,
  getMatIconFailedToSanitizeUrlError,
  getMatIconNameNotFoundError,
  getMatIconNoHttpProviderError
};
//# sourceMappingURL=@angular_material_icon.js.map
