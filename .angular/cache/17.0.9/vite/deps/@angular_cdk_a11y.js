import {
  A11yModule,
  ActiveDescendantKeyManager,
  AriaDescriber,
  CDK_DESCRIBEDBY_HOST_ATTRIBUTE,
  CDK_DESCRIBEDBY_ID_PREFIX,
  CdkAriaLive,
  CdkMonitorFocus,
  CdkTrapFocus,
  ConfigurableFocusTrap,
  ConfigurableFocusTrapFactory,
  EventListenerFocusTrapInertStrategy,
  FOCUS_MONITOR_DEFAULT_OPTIONS,
  FOCUS_TRAP_INERT_STRATEGY,
  FocusKeyManager,
  FocusMonitor,
  FocusTrap,
  FocusTrapFactory,
  HighContrastModeDetector,
  INPUT_MODALITY_DETECTOR_DEFAULT_OPTIONS,
  INPUT_MODALITY_DETECTOR_OPTIONS,
  InputModalityDetector,
  InteractivityChecker,
  IsFocusableConfig,
  LIVE_ANNOUNCER_DEFAULT_OPTIONS,
  LIVE_ANNOUNCER_ELEMENT_TOKEN,
  LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY,
  ListKeyManager,
  LiveAnnouncer,
  MESSAGES_CONTAINER_ID,
  addAriaReferencedId,
  getAriaReferenceIds,
  isFakeMousedownFromScreenReader,
  isFakeTouchstartFromScreenReader,
  removeAriaReferencedId
} from "./chunk-GARXXTCJ.js";
import "./chunk-QEKODB4M.js";
import "./chunk-NN72S56S.js";
import "./chunk-MCBQ7T4L.js";
import "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  A11yModule,
  ActiveDescendantKeyManager,
  AriaDescriber,
  CDK_DESCRIBEDBY_HOST_ATTRIBUTE,
  CDK_DESCRIBEDBY_ID_PREFIX,
  CdkAriaLive,
  CdkMonitorFocus,
  CdkTrapFocus,
  ConfigurableFocusTrap,
  ConfigurableFocusTrapFactory,
  EventListenerFocusTrapInertStrategy,
  FOCUS_MONITOR_DEFAULT_OPTIONS,
  FOCUS_TRAP_INERT_STRATEGY,
  FocusKeyManager,
  FocusMonitor,
  FocusTrap,
  FocusTrapFactory,
  HighContrastModeDetector,
  INPUT_MODALITY_DETECTOR_DEFAULT_OPTIONS,
  INPUT_MODALITY_DETECTOR_OPTIONS,
  InputModalityDetector,
  InteractivityChecker,
  IsFocusableConfig,
  LIVE_ANNOUNCER_DEFAULT_OPTIONS,
  LIVE_ANNOUNCER_ELEMENT_TOKEN,
  LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY,
  ListKeyManager,
  LiveAnnouncer,
  MESSAGES_CONTAINER_ID,
  addAriaReferencedId,
  getAriaReferenceIds,
  isFakeMousedownFromScreenReader,
  isFakeTouchstartFromScreenReader,
  removeAriaReferencedId
};
//# sourceMappingURL=@angular_cdk_a11y.js.map
