import {
  MAT_ERROR,
  MAT_FORM_FIELD,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MAT_PREFIX,
  MAT_SUFFIX,
  MatError,
  MatFormField,
  MatFormFieldControl,
  MatFormFieldModule,
  MatHint,
  MatLabel,
  MatPrefix,
  MatSuffix,
  getMatFormFieldDuplicatedHintError,
  getMatFormFieldMissingControlError,
  getMatFormFieldPlaceholderConflictError,
  matFormFieldAnimations
} from "./chunk-RL4HLGNP.js";
import "./chunk-JLD3QCX6.js";
import "./chunk-2M4B6PU2.js";
import "./chunk-7PLXW5PR.js";
import "./chunk-55OKLQ4U.js";
import "./chunk-4L62CIWV.js";
import "./chunk-GARXXTCJ.js";
import "./chunk-QEKODB4M.js";
import "./chunk-NN72S56S.js";
import "./chunk-4WQH3DGR.js";
import "./chunk-MCBQ7T4L.js";
import "./chunk-GLDRZ4VJ.js";
import "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";
export {
  MAT_ERROR,
  MAT_FORM_FIELD,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MAT_PREFIX,
  MAT_SUFFIX,
  MatError,
  MatFormField,
  MatFormFieldControl,
  MatFormFieldModule,
  MatHint,
  MatLabel,
  MatPrefix,
  MatSuffix,
  getMatFormFieldDuplicatedHintError,
  getMatFormFieldMissingControlError,
  getMatFormFieldPlaceholderConflictError,
  matFormFieldAnimations
};
//# sourceMappingURL=@angular_material_form-field.js.map
