import {
  Actions,
  NGXS_PLUGINS,
  Store,
  getActionTypeFromInstance,
  getValue,
  ofActionDispatched,
  setValue
} from "./chunk-DGU5BOYB.js";
import {
  FormGroupDirective,
  ReactiveFormsModule
} from "./chunk-IYDN2AE5.js";
import "./chunk-GLDRZ4VJ.js";
import {
  ChangeDetectorRef,
  Directive,
  Injectable,
  Input,
  NgModule,
  setClassMetadata,
  ɵɵdefineDirective,
  ɵɵdefineInjectable,
  ɵɵdefineInjector,
  ɵɵdefineNgModule,
  ɵɵdirectiveInject
} from "./chunk-NMY3SVSW.js";
import "./chunk-7YR2YE5A.js";
import "./chunk-PQU7T2OG.js";
import {
  Subject,
  debounceTime,
  distinctUntilChanged,
  filter,
  takeUntil
} from "./chunk-GZKOQ4LB.js";
import "./chunk-UAZK63YF.js";
import "./chunk-XEMAJPGH.js";

// node_modules/@ngxs/form-plugin/fesm2015/ngxs-form-plugin.js
var UpdateFormStatus = class {
  constructor(payload) {
    this.payload = payload;
  }
};
UpdateFormStatus.type = "[Forms] Update Form Status";
var UpdateFormValue = class {
  constructor(payload) {
    this.payload = payload;
  }
};
UpdateFormValue.type = "[Forms] Update Form Value";
var UpdateForm = class {
  constructor(payload) {
    this.payload = payload;
  }
};
UpdateForm.type = "[Forms] Update Form";
var UpdateFormDirty = class {
  constructor(payload) {
    this.payload = payload;
  }
};
UpdateFormDirty.type = "[Forms] Update Form Dirty";
var SetFormDirty = class {
  constructor(payload) {
    this.payload = payload;
  }
};
SetFormDirty.type = "[Forms] Set Form Dirty";
var SetFormPristine = class {
  constructor(payload) {
    this.payload = payload;
  }
};
SetFormPristine.type = "[Forms] Set Form Pristine";
var UpdateFormErrors = class {
  constructor(payload) {
    this.payload = payload;
  }
};
UpdateFormErrors.type = "[Forms] Update Form Errors";
var SetFormDisabled = class {
  constructor(payload) {
    this.payload = payload;
  }
};
SetFormDisabled.type = "[Forms] Set Form Disabled";
var SetFormEnabled = class {
  constructor(payload) {
    this.payload = payload;
  }
};
SetFormEnabled.type = "[Forms] Set Form Enabled";
var ResetForm = class {
  constructor(payload) {
    this.payload = payload;
  }
};
ResetForm.type = "[Forms] Reset Form";
var NgxsFormPlugin = class {
  handle(state, event, next) {
    const type = getActionTypeFromInstance(event);
    let nextState = state;
    if (type === UpdateFormValue.type || type === UpdateForm.type || type === ResetForm.type) {
      const {
        value
      } = event.payload;
      const payloadValue = Array.isArray(value) ? value.slice() : isObjectLike(value) ? Object.assign({}, value) : value;
      const path = this.joinPathWithPropertyPath(event);
      nextState = setValue(nextState, path, payloadValue);
    }
    if (type === ResetForm.type) {
      const model = getValue(nextState, `${event.payload.path}.model`);
      nextState = setValue(nextState, `${event.payload.path}`, {
        model
      });
    }
    if (type === UpdateFormStatus.type || type === UpdateForm.type) {
      nextState = setValue(nextState, `${event.payload.path}.status`, event.payload.status);
    }
    if (type === UpdateFormErrors.type || type === UpdateForm.type) {
      nextState = setValue(nextState, `${event.payload.path}.errors`, Object.assign({}, event.payload.errors));
    }
    if (type === UpdateFormDirty.type || type === UpdateForm.type) {
      nextState = setValue(nextState, `${event.payload.path}.dirty`, event.payload.dirty);
    }
    if (type === SetFormDirty.type) {
      nextState = setValue(nextState, `${event.payload}.dirty`, true);
    }
    if (type === SetFormPristine.type) {
      nextState = setValue(nextState, `${event.payload}.dirty`, false);
    }
    if (type === SetFormDisabled.type) {
      nextState = setValue(nextState, `${event.payload}.disabled`, true);
    }
    if (type === SetFormEnabled.type) {
      nextState = setValue(nextState, `${event.payload}.disabled`, false);
    }
    return next(nextState, event);
  }
  joinPathWithPropertyPath({
    payload
  }) {
    let path = `${payload.path}.model`;
    if (payload.propertyPath) {
      path += `.${payload.propertyPath}`;
    }
    return path;
  }
};
NgxsFormPlugin.ɵfac = function NgxsFormPlugin_Factory(t) {
  return new (t || NgxsFormPlugin)();
};
NgxsFormPlugin.ɵprov = ɵɵdefineInjectable({
  token: NgxsFormPlugin,
  factory: NgxsFormPlugin.ɵfac
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(NgxsFormPlugin, [{
    type: Injectable
  }], null, null);
})();
function isObjectLike(target) {
  return target !== null && typeof target === "object";
}
var FormDirective = class {
  constructor(_actions$, _store, _formGroupDirective, _cd) {
    this._actions$ = _actions$;
    this._store = _store;
    this._formGroupDirective = _formGroupDirective;
    this._cd = _cd;
    this.path = null;
    this._debounce = 100;
    this._clearDestroy = false;
    this._updating = false;
    this._destroy$ = new Subject();
  }
  set debounce(debounce) {
    this._debounce = Number(debounce);
  }
  get debounce() {
    return this._debounce;
  }
  set clearDestroy(val) {
    this._clearDestroy = val != null && `${val}` !== "false";
  }
  get clearDestroy() {
    return this._clearDestroy;
  }
  ngOnInit() {
    this._actions$.pipe(ofActionDispatched(ResetForm), filter((action) => action.payload.path === this.path), takeUntil(this._destroy$)).subscribe(({
      payload: {
        value
      }
    }) => {
      this.form.reset(value);
      this.updateFormStateWithRawValue(true);
      this._cd.markForCheck();
    });
    this.getStateStream(`${this.path}.model`).subscribe((model) => {
      if (this._updating || !model) {
        return;
      }
      this.form.patchValue(model);
      this._cd.markForCheck();
    });
    this.getStateStream(`${this.path}.dirty`).subscribe((dirty) => {
      if (this.form.dirty === dirty || typeof dirty !== "boolean") {
        return;
      }
      if (dirty) {
        this.form.markAsDirty();
      } else {
        this.form.markAsPristine();
      }
      this._cd.markForCheck();
    });
    this._store.selectOnce((state) => getValue(state, this.path)).subscribe(() => {
      this._store.dispatch([new UpdateFormValue({
        path: this.path,
        value: this.form.getRawValue()
      }), new UpdateFormStatus({
        path: this.path,
        status: this.form.status
      }), new UpdateFormDirty({
        path: this.path,
        dirty: this.form.dirty
      })]);
    });
    this.getStateStream(`${this.path}.disabled`).subscribe((disabled) => {
      if (this.form.disabled === disabled || typeof disabled !== "boolean") {
        return;
      }
      if (disabled) {
        this.form.disable();
      } else {
        this.form.enable();
      }
      this._cd.markForCheck();
    });
    this._formGroupDirective.valueChanges.pipe(distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)), this.debounceChange()).subscribe(() => {
      this.updateFormStateWithRawValue();
    });
    this._formGroupDirective.statusChanges.pipe(distinctUntilChanged(), this.debounceChange()).subscribe((status) => {
      this._store.dispatch(new UpdateFormStatus({
        status,
        path: this.path
      }));
    });
  }
  updateFormStateWithRawValue(withFormStatus) {
    if (this._updating)
      return;
    const value = this._formGroupDirective.control.getRawValue();
    const actions = [new UpdateFormValue({
      path: this.path,
      value
    }), new UpdateFormDirty({
      path: this.path,
      dirty: this._formGroupDirective.dirty
    }), new UpdateFormErrors({
      path: this.path,
      errors: this._formGroupDirective.errors
    })];
    if (withFormStatus) {
      actions.push(new UpdateFormStatus({
        path: this.path,
        status: this._formGroupDirective.status
      }));
    }
    this._updating = true;
    this._store.dispatch(actions).subscribe({
      error: () => this._updating = false,
      complete: () => this._updating = false
    });
  }
  ngOnDestroy() {
    this._destroy$.next();
    if (this.clearDestroy) {
      this._store.dispatch(new UpdateForm({
        path: this.path,
        value: null,
        dirty: null,
        status: null,
        errors: null
      }));
    }
  }
  debounceChange() {
    const skipDebounceTime = this._formGroupDirective.control.updateOn !== "change" || this._debounce < 0;
    return skipDebounceTime ? (change) => change.pipe(takeUntil(this._destroy$)) : (change) => change.pipe(debounceTime(this._debounce), takeUntil(this._destroy$));
  }
  get form() {
    return this._formGroupDirective.form;
  }
  getStateStream(path) {
    return this._store.select((state) => getValue(state, path)).pipe(takeUntil(this._destroy$));
  }
};
FormDirective.ɵfac = function FormDirective_Factory(t) {
  return new (t || FormDirective)(ɵɵdirectiveInject(Actions), ɵɵdirectiveInject(Store), ɵɵdirectiveInject(FormGroupDirective), ɵɵdirectiveInject(ChangeDetectorRef));
};
FormDirective.ɵdir = ɵɵdefineDirective({
  type: FormDirective,
  selectors: [["", "ngxsForm", ""]],
  inputs: {
    path: ["ngxsForm", "path"],
    debounce: ["ngxsFormDebounce", "debounce"],
    clearDestroy: ["ngxsFormClearOnDestroy", "clearDestroy"]
  }
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(FormDirective, [{
    type: Directive,
    args: [{
      selector: "[ngxsForm]"
    }]
  }], function() {
    return [{
      type: Actions
    }, {
      type: Store
    }, {
      type: FormGroupDirective
    }, {
      type: ChangeDetectorRef
    }];
  }, {
    path: [{
      type: Input,
      args: ["ngxsForm"]
    }],
    debounce: [{
      type: Input,
      args: ["ngxsFormDebounce"]
    }],
    clearDestroy: [{
      type: Input,
      args: ["ngxsFormClearOnDestroy"]
    }]
  });
})();
var NgxsFormPluginModule = class _NgxsFormPluginModule {
  static forRoot() {
    return {
      ngModule: _NgxsFormPluginModule,
      providers: [{
        provide: NGXS_PLUGINS,
        useClass: NgxsFormPlugin,
        multi: true
      }]
    };
  }
};
NgxsFormPluginModule.ɵfac = function NgxsFormPluginModule_Factory(t) {
  return new (t || NgxsFormPluginModule)();
};
NgxsFormPluginModule.ɵmod = ɵɵdefineNgModule({
  type: NgxsFormPluginModule,
  declarations: [FormDirective],
  imports: [ReactiveFormsModule],
  exports: [FormDirective]
});
NgxsFormPluginModule.ɵinj = ɵɵdefineInjector({
  imports: [[ReactiveFormsModule]]
});
(() => {
  (typeof ngDevMode === "undefined" || ngDevMode) && setClassMetadata(NgxsFormPluginModule, [{
    type: NgModule,
    args: [{
      imports: [ReactiveFormsModule],
      declarations: [FormDirective],
      exports: [FormDirective]
    }]
  }], null, null);
})();
export {
  NgxsFormPlugin,
  NgxsFormPluginModule,
  ResetForm,
  SetFormDirty,
  SetFormDisabled,
  SetFormEnabled,
  SetFormPristine,
  UpdateForm,
  UpdateFormDirty,
  UpdateFormErrors,
  UpdateFormStatus,
  UpdateFormValue,
  FormDirective as ɵFormDirective
};
//# sourceMappingURL=@ngxs_form-plugin.js.map
