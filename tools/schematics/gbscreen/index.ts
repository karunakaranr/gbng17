import {
  apply,
  filter,
  MergeStrategy,
  chain,
  externalSchematic,
  mergeWith,
  move,
  noop,
  Rule,
  schematic,
  SchematicContext,
  template,
  Tree,
  url,
} from '@angular-devkit/schematics';
import { join, normalize } from 'path';
import { getWorkspace } from '@schematics/angular/utility/config';
// import { getWorkspace } from '@schematics/angular/utility/workspace';
import { strings } from '@angular-devkit/core';
import { getProjectConfig } from '@nrwl/workspace';

export interface SchematicOptions {
  name: string;
  project: string;
  path?: string;
  hasScreenMetadata: boolean;
  screenid: string;
  needDataAccess: boolean;
  serviceoptions?: string;

  tsext: string;
  skipService?: boolean;
  skipDBService?: boolean;
}

export default function (schema: SchematicOptions): Rule {
  // return chain([
  //   externalSchematic('@nrwl/workspace', 'lib', {
  //     name: schema.name,
  //   }),
  // ]);

  // return (tree: Tree, _context: SchematicContext) => {
  //   setupOptions(tree, schema);

  //   const movePath = normalize(schema.path + '/');
  //   const templateSource = apply(url('./files'), [
  //     schema.skipService ? filter(path => !(path.endsWith('.service.ts') && !path.endsWith('db.service.ts'))) : noop(),
  //     schema.skipDBService ? filter(path => !path.endsWith('db.service.ts')) : noop(),
  //     template({...schema, ...strings}),
  //     move(movePath)
  //   ]);
  //   const rule = mergeWith(templateSource, MergeStrategy.Overwrite);
  //   return rule(tree, _context);
  // };

  return (tree: Tree, context: SchematicContext) => {
    return chain([generateFiles(schema)])(
      tree,
      context
    );
  };
}

function generateFiles(schema: SchematicOptions): Rule {
  return (tree: Tree, context: SchematicContext) => {
    setupOptions(tree, schema);

    const movePath = normalize(schema.path + '/');
    const templateSource = apply(url('./files'), [
      schema.skipService ? filter(path => !(path.endsWith('.service.__tsext__') && !path.endsWith('db.service.__tsext__'))) : noop(),
      schema.skipDBService ? filter(path => !path.endsWith('db.service.__tsext__')) : noop(),
      template({...schema, ...strings}),
      move(movePath),
    ]);
    context.logger.info('************************************************');
    context.logger.info('*** DONT FORGET TO MODIFY THE MODULE FILE... ***');
    context.logger.info('************************************************');

    return chain([mergeWith(templateSource)])(tree, context);
  };
}

function setupOptions(host: Tree, options: SchematicOptions): Tree {
  const workspace = getWorkspace(host);
  const project = workspace.projects[options.project as string];

  // const project = workspace.projects[options.projectname];

  if (options.path === undefined && project) {
    options.path = join(normalize(project.root), 'src/app'); // buildDefaultPath(project);
  }
  else {
    options.path = join(normalize(project.root), 'src/app', options.path); // buildDefaultPath(project);
  }

  options.skipService = options.serviceoptions === 'none' || options.serviceoptions === 'dbserv' ? true : false;
  options.skipDBService = options.serviceoptions === 'none' || options.serviceoptions === 'serv' ? true : false;

  options.tsext = "ts";

  return host;
}

